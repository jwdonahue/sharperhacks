﻿// Copyright (c) Joseph W Donahue dba SharperHacks.org
// https://www.sharperhacks.org
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SharperHacks.Diagnostics.Constraints.UnitTests
{
    [TestClass]
    public class NotNullTSmokeTests
    {
        [TestMethod]
        public void TNotNull()
        {
            string value = "FF9BF14D75C5448720D04B75F9DC0195";
            NotNull<string> str = (NotNull<string>)value; // Exercise explicit conversion.
            
            Assert.IsNotNull(str.Value);
            Assert.AreEqual(value, str); // Exercise implicit conversion.
        }

        [TestMethod]
        [ExpectedException(typeof(VerifyException))]
        public void TIsNull()
        {
            NotNull<string> str = new NotNull<string>(null); // Will throw.
        }
    }
}