// Copyright (c) Joseph W Donahue dba SharperHacks.org
// https://www.sharperhacks.org
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SharperHacks.Diagnostics.Constraints.UnitTests
{
    [TestClass]
    public class VerifySmokeTests
    {
        // It turns out that which-ever of these test methods comes first, that's the one that has
        // a huge run-time cost of 18-51 ms. When second or third, the AreNotNull_SmokeTest runs <1 ms
        // in my environment.

        [TestMethod]
        public void AreNotNull_SmokeTest()
        {
            bool exceptionCaught = false;

            try
            {
                object[] array = {
                    new object(),
                    new object(),
                    new object()
                };
                string comment = "34CB28C4500B4C8AAD64C694FB4B1EA1";

                Verify.AreNotNull(array, comment: comment);
            }
            catch (VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.AreNotNull(null);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("@ AreNotNull_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(43)"));
            }
            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            string commentZ = "E43C49E9C4BF451D1B63AA220BC02490";

            try
            {
                object[] array = {
                    new object(),
                    new object(),
                    null
                };

                Verify.AreNotNull(array, comment: commentZ);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("@ AreNotNull_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(69)"));
                Assert.IsTrue(exceptionMessage.Contains(commentZ));
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void AreNotNullOrEmpty_SmokeTest()
        {
            bool exceptionCaught = false;

            try
            {
                string[] array = {
                    new string("Not empty string."),
                    new string("Not empty string."),
                    new string("Not empty string.")
                };
                string comment = "525BE3136E0A4B9464FE49FBADD473B8";
                
                Verify.AreNotNullOrEmpty(array, comment: comment);
            }
            catch (VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.AreNotNullOrEmpty(null);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNullOrEmpty"));
                Assert.IsTrue(exceptionMessage.Contains("@ AreNotNullOrEmpty_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(109)"));
            }
            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            try
            {
                string[] array = {
                    new string("Not empty string."),
                    new string("Not empty string."),
                    null
                };

                Verify.AreNotNullOrEmpty(array);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("AreNotNullOrEmpty"));
                Assert.IsTrue(exceptionMessage.Contains("@ AreNotNullOrEmpty_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(134)"));
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void IsNotNull_SmokeTest()
        {
            bool exceptionCaught = false;

            try
            {
                string str = new string("testing...");
                Verify.IsNotNull(str);
            }
            catch(VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.IsNotNull(null);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("@ IsNotNull_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(168)"));
            }
            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            string comment = "4775901B285747888824965B89EC76B5";
            try
            {
                string str = new string("testing...");
                Verify.IsNotNull(str, comment: comment);
            }
            catch(VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.IsNotNull(null, comment: comment);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNull"));
                Assert.IsTrue(exceptionMessage.Contains("@ IsNotNull_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(196)"));
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void IsNotNullOrEmpty_SmokeTest()
        {
            bool exceptionCaught = false;

            try
            {
                string str = new string("testing...");
                Verify.IsNotNullOrEmpty(str);
            }
            catch (VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                string str = new string("");
                Verify.IsNotNullOrEmpty(str);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.StringIsEmptyExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNullOrEmpty"));
                Assert.IsTrue(exceptionMessage.Contains("@ IsNotNullOrEmpty_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(229)"));
            }
            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            try
            {
                Verify.IsNotNullOrEmpty(null);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.IsNotNullExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("IsNotNullOrEmpty"));
                Assert.IsTrue(exceptionMessage.Contains("@ IsNotNullOrEmpty_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(246)"));
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void IsTrue_SmokeTest()
        {
            bool exceptionCaught = false;
            string comment = "My special comment: 55AEFBB4272641057D5A638C31837485.";

            try
            {
                Verify.IsTrue(true);
            }
            catch(VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.IsTrue(false, comment);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.ExpressionNotTrueExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains(comment));
                Assert.IsTrue(exceptionMessage.Contains("@ IsTrue_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(278)"));
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void IsFalse_SmokeTest()
        {
            bool exceptionCaught = false;

            try
            {
                Verify.IsFalse(false);
            }
            catch (VerifyException ex)
            {
                Assert.Fail("Caught unexpected exception: " + ex.Message);
            }

            try
            {
                Verify.IsFalse(true);
            }
            catch (VerifyException ex)
            {
                exceptionCaught = true;
                string exceptionMessage = ex.ToString();
                Assert.IsTrue(exceptionMessage.Contains(Verify.ExpressionNotFalseExceptionPrefix));
                Assert.IsTrue(exceptionMessage.Contains("@ IsFalse_SmokeTest"));
                Assert.IsTrue(exceptionMessage.Contains("VerifySmokeTests.cs"));
                Assert.IsTrue(exceptionMessage.Contains("(309)"));
            }
            Assert.IsTrue(exceptionCaught);
        }
    }
}
