﻿// Copyright (c) Joseph W Donahue dba SharperHacks.org
// https://www.sharperhacks.org
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SharperHacks.Diagnostics.Constraints.UnitTests
{
    [TestClass]
    public class VerifyExceptionSmokeTests
    {
        [TestMethod]
        public void VerifyException_DefaultConstructor_SmokeTest()
        {
            VerifyException ex = new VerifyException();
            Assert.IsNotNull(ex);
            Assert.IsTrue(ex.Message.Contains("SharperHacks.Diagnostics.VerifyException"));

            bool exceptionCaught;
            try
            {
                throw ex;
            }
            catch (Exception caught)
            {
                Assert.IsTrue(caught.Message.Contains("SharperHacks.Diagnostics.VerifyException"));
                exceptionCaught = true;
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void VerifyException_MessageConstructor_SmokeTest()
        {
            string msg = "Test message.";

            VerifyException ex = new VerifyException(msg);
            Assert.IsNotNull(ex);
            Assert.IsTrue(ex.Message.Contains(msg));

            bool exceptionCaught;
            try
            {
                throw ex;
            }
            catch (VerifyException caught)
            {
                Assert.IsTrue(caught.Message.Contains(msg));
                exceptionCaught = true;
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void VerifyException_MessageAndInnerConstructor_SmokeTest()
        {
            string innerMsg = "Inner message.";
            string outerMsg = "Outer message.";

            VerifyException innerEx = new VerifyException(innerMsg);
            Assert.IsNotNull(innerEx);
            Assert.IsTrue(innerEx.Message.Contains(innerMsg));

            VerifyException outerEx = new VerifyException(outerMsg, innerEx);
            Assert.IsNotNull(outerEx);
            Assert.IsTrue(outerEx.InnerException.Message.Contains(innerMsg));
            Assert.IsTrue(outerEx.Message.Contains(outerMsg));

            bool exceptionCaught;
            try
            {
                throw outerEx;
            }
            catch (Exception caught)
            {
                Assert.IsTrue(outerEx.InnerException.Message.Contains(innerMsg));
                Assert.IsTrue(caught.Message.Contains(outerMsg));
                exceptionCaught = true;
            }
            Assert.IsTrue(exceptionCaught);
        }

        [TestMethod]
        public void VerifyException_SerializationConstructor_SmokeTest()
        {
            var innerEx = new Exception("foo");
            var originalException = new VerifyException("message", innerEx);
            var buffer = new byte[4096];
            var ms = new MemoryStream(buffer);
            var ms2 = new MemoryStream(buffer);
            var formatter = new BinaryFormatter();

            formatter.Serialize(ms, originalException);
            var deserializedException = (VerifyException)formatter.Deserialize(ms2);

            Assert.AreEqual(originalException.InnerException.Message, deserializedException.InnerException.Message);
            Assert.AreEqual(originalException.Message, deserializedException.Message);
        }
    }
}