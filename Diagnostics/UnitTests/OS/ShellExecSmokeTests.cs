﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.IO;
using System.Diagnostics;

namespace SharperHacks.Diagnostics.OS.UnitTests
{
    [TestClass]
    public class ShellExecSmokeTests
    {
        [TestMethod]
        public void RunSyncSmokeTest()
        {
            using (var tempDir = new TempDirectory())
            {
                var cmd = "cmd.exe";
                var args = $"/c dir {tempDir.DirectoryInfo.FullName}";
                var se1 = new ShellExec(cmd, args);

                Assert.AreEqual(string.Empty, se1.Result);

                var resultCode = se1.RunSync();

                Assert.AreEqual(0, resultCode);
                Assert.IsNotNull(se1.Result);
                Assert.AreNotEqual(string.Empty, se1.Result);
                Assert.IsTrue(se1.Result.Contains($"Directory of {tempDir.DirectoryInfo.FullName}"));

                var psi = new ProcessStartInfo(cmd, args)
                {
                    RedirectStandardOutput = true,  // Capture output.
                    UseShellExecute = false,        // No graphical shell.
                    CreateNoWindow = true           // Execute in background (no window).
                };

                var se2 = new ShellExec(psi);

                Assert.AreEqual(string.Empty, se2.Result);

                resultCode = se2.RunSync();

                Assert.AreEqual(0, resultCode);
                Assert.IsNotNull(se2.Result);
                Assert.AreNotEqual(string.Empty, se2.Result);
                Assert.IsTrue(se2.Result.Contains($"Directory of {tempDir.DirectoryInfo.FullName}"));
            }
        }
    }
}
