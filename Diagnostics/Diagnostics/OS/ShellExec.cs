﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Diagnostics;

// WIP: This class is incomplete in design and implementation.

namespace SharperHacks.Diagnostics.OS
{
    /// <summary>
    /// Wrapper class for running separate processes, and capturing their output.
    /// </summary>
    public class ShellExec
    {
        #region public

        /// <summary>
        /// The args to run Cmd with.
        /// </summary>
        public string Args { get; set; }

        /// <summary>
        /// The command to execute.
        /// </summary>
        public string Cmd { get; set; }

        /// <summary>
        /// The ProcessStartInfo used to execute the command.
        /// </summary>
        /// <remarks>
        /// Initialized by the constructors. Can be modified before calling
        /// RunSync() or RunAsync().
        /// </remarks>
        public ProcessStartInfo ProcessStartInfo { get; set; }

        /// <summary>
        /// The Process object used to execute the command.
        /// </summary>
        /// <remarks>
        /// Initialized by the constructors. Can be modified before calling
        /// RunSync() or RunAsync().
        /// </remarks>
        public Process Process { get; set; }

        /// <summary>
        /// The captured output from the last command execution.
        /// </summary>
        public string Result
        {
            get => _result;
        }

        /// <summary>
        /// Run Cmd with Args, synchronously.
        /// </summary>
        /// <returns></returns>
        public int RunSync()
        {
            Process.Start();
            _result = Process.StandardOutput.ReadToEnd();
            return Process.ExitCode;
        }

#if false
        /// <summary>
        /// Run Cmd with Args, asynchronously.
        /// </summary>
        /// <returns></returns>
        public void RunAsync()
        {
            return 0;
        }
#endif
        #region Constructors

        /// <summary>
        /// Simple constructor, uses default ProcessStartInfo settings and adds cmd and args.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="args"></param>
        public ShellExec(string cmd, string args)
        {
            var psi = new ProcessStartInfo(Cmd, Args)
            {
                RedirectStandardOutput = true,  // Capture output.
                UseShellExecute = false,        // No graphical shell.
                CreateNoWindow = true           // Execute in background (no window).
            };

            psi.FileName = Cmd = cmd;
            psi.Arguments = Args = args;
            ProcessStartInfo = psi;

            Process = new Process
            {
                StartInfo = ProcessStartInfo
            };

            _result = string.Empty;
        }

        /// <summary>
        /// Construct an instance from an initialized ProcessStartInfo object.
        /// </summary>
        /// <param name="psi"></param>
        public ShellExec(ProcessStartInfo psi)
        {
            Cmd = psi.FileName;
            Args = psi.Arguments;
            ProcessStartInfo = psi;

            Process = new Process
            {
                StartInfo = ProcessStartInfo
            };

            _result = string.Empty;
        }

        #endregion Constructors

        #endregion public

        #region private

        private string _result;
        #endregion private
    }
}
