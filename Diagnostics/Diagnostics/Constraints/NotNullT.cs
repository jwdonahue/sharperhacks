﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

namespace SharperHacks.Diagnostics.Constraints
{
    // TODO: Should investigate new constraints features in latest C#.
    // THis is likely to be obsolete in C# 3.x.  We'll adapt accordingly,
    // but support this for as long we support .Net Core 2.2+.
    // This might not be part of the SharperHacks.*.Core3.x assemblies.

    /// <summary>
    /// Ensure T is not null.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class NotNull<T>
    {
        /// <summary>
        /// Get the value of T.
        /// </summary>
        public T Value { get; }

        /// <summary>
        /// Implicit conversion from NotNull&lt; T &gt; to T.
        /// </summary>
        /// <param name="v"></param>
        public static implicit operator T(NotNull<T> v) => v.Value;

        /// <summary>
        /// Explicit conversion from T to NotNull&lt; T &gt;.
        /// </summary>
        /// <param name="v"></param>
        public static explicit operator NotNull<T>(T v) => new NotNull<T>(v);

        /// <summary>
        /// The constructor.
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="VerifyException"/>
        public NotNull(T value)
        {
            Verify.IsNotNull(value);
            Value = value;
        }
    }
}