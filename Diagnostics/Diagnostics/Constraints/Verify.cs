﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Runtime.CompilerServices;

// ReSharper disable once CheckNamespace
namespace SharperHacks.Diagnostics.Constraints
{
    /// <summary>
    /// The Verify class provides static members that provide always-on run-time verification of expected invariants.
    /// </summary>
    public static class Verify
    {
        // These string constants are primarily intended for unit testing purposes.

        /// <summary>
        /// All of the *NotNull methods throw a VerifyException that starts with this string.
        /// </summary>
        public const string IsNotNullExceptionPrefix = "Verify failed, value is NULL.";

        /// <summary>
        /// All of the *Empty(string) methods throw a VerifyException that starts with this string.
        /// </summary>
        public const string StringIsEmptyExceptionPrefix = "Verify failed, string length is zero.";

        /// <summary>
        /// All of the IsFalse methods throw a VerifyException that starts with this string.
        /// </summary>
        public const string ExpressionNotFalseExceptionPrefix = "Verify failed, expression result not false.";

        /// <summary>
        /// All of the IsTrue methods throw a VerifyException that starts with this string.
        /// </summary>
        public const string ExpressionNotTrueExceptionPrefix = "Verify failed, expression result not true.";

        /// <summary>
        /// Verify that the array and its members are not null.
        /// </summary>
        /// <param name="array">Array to check.</param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="array"/> or any of the array elements are null.
        /// </exception>
        public static void AreNotNull(
            object[] array,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
        ) => AreNotNullImp(array, null, memberName, fileName, lineNumber);

        /// <summary>
        /// Verify that the array and its members are not null.
        /// </summary>
        /// <param name="array">Array to check.</param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// NOTE: That calling this method with object and string is ambiguous until you name
        /// the comment argument (ex: comment: "your comment")
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="array"/> or any of the array elements are null.
        /// </exception>
        public static void AreNotNull(
            object[] array,
            string comment,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
        ) => AreNotNullImp(array, comment, memberName, fileName, lineNumber);

        private static void AreNotNullImp(
            object[] array, 
            string comment,
            string memberName = "",
            string fileName = "",
            int lineNumber = 0)
        {
            IsNotNullImp(array, comment, memberName, fileName, lineNumber);

            // Arrays must have at least one element, so we don't check for zero array.Length here.

            foreach (var item in array)
            {
                IsNotNullImp(item, comment, memberName, fileName, lineNumber);
            }
        }

        /// <summary>
        /// Verify the array of strings is not null and its members are not null or empty.
        /// </summary>
        /// <param name="array">Array to check.</param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="array"/> or if any of the <paramref name="array"/> elements are null or empty.
        /// </exception>
        public static void AreNotNullOrEmpty(
            string[] array,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            ) => AreNotNullOrEmptyImp(array, null, memberName, fileName, lineNumber);

        /// <summary>
        /// Verify the array of strings is not null and its members are not null or empty.
        /// </summary>
        /// <param name="array">Array to check.</param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="array"/> or if any of the <paramref name="array"/> elements are null or empty.
        /// </exception>
        public static void AreNotNullOrEmpty(
            string[] array,
            string comment,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
        ) => AreNotNullOrEmptyImp(array, comment, memberName, fileName, lineNumber);

        private static void AreNotNullOrEmptyImp(
            string[] array, 
            string comment,
            string memberName = "",
            string fileName = "",
            int lineNumber = 0)
        {
            IsNotNullImp(array, comment, memberName, fileName, lineNumber);

            // Arrays must have at least one element, so we don't check array.Length here.

            foreach (var item in array)
            {
                IsNotNullOrEmptyImp(item, comment, memberName, fileName, lineNumber);
            }
        }

        /// <summary>
        /// Verify that an object reference is not null.
        /// </summary>
        /// <param name="value">String to check.</param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="value"/> is null.
        /// </exception>
        public static void IsNotNull(
            object value,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            ) => IsNotNullImp(value, null, memberName, fileName, lineNumber);

        /// <summary>
        /// Verify that an object reference is not null.
        /// </summary>
        /// <param name="value">String to check.</param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="value"/> is null.
        /// </exception>
        public static void IsNotNull(
            object value,
            string comment,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
        ) => IsNotNullImp(value, comment, memberName, fileName, lineNumber);

        private static void IsNotNullImp(
            object value, 
            string comment,
            string memberName = "",
            string fileName = "",
            int lineNumber = 0)
        {
            if (value == null)
            {
                throw new VerifyException(
                    BuildExceptionMessage(
                        IsNotNullExceptionPrefix, 
                        comment,
                        memberName,
                        fileName,
                        lineNumber
                    ));
            }
        }

        /// <summary>
        /// Verify <paramref name="value"/> string is not null or empty.
        /// </summary>
        /// <param name="value">String to check.</param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="value"/> is null or empty.
        /// </exception>
        public static void IsNotNullOrEmpty(
            string value,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            ) => IsNotNullOrEmpty(value, null, memberName, fileName, lineNumber);

        /// <summary>
        /// Verify <paramref name="value"/> string is not null or empty.
        /// </summary>
        /// <param name="value">String to check.</param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="value"/> is null or empty.
        /// </exception>
        public static void IsNotNullOrEmpty(
            string value,
            string comment,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
        ) => IsNotNullOrEmptyImp(value, comment, memberName, fileName, lineNumber);

        private static void IsNotNullOrEmptyImp(
            string value, 
            string comment,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            )
        {
            IsNotNullImp(value, comment, memberName, fileName, lineNumber);

            if (value.Length == 0)
            {
                throw new VerifyException(
                    BuildExceptionMessage(
                        StringIsEmptyExceptionPrefix, 
                        comment,
                        memberName,
                        fileName,
                        lineNumber
                        ));
            }
        }

        /// <summary>
        /// Verify that <paramref name="expressionResult"/> is true.
        /// </summary>
        /// <param name="expressionResult"></param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="expressionResult"/> is not true.
        /// </exception>
        public static void IsTrue(
            bool expressionResult, 
            string comment = null,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            )
        {
            if (!expressionResult)
            {
                throw new VerifyException(
                    BuildExceptionMessage(
                        ExpressionNotTrueExceptionPrefix, 
                        comment,
                        memberName,
                        fileName,
                        lineNumber));
            }
        }

        /// <summary>
        /// Verify that <paramref name="expressionResult"/> is false.
        /// </summary>
        /// <param name="expressionResult"></param>
        /// <param name="comment">
        /// Any additional information to tag onto the exception message. May be null or empty.
        /// </param>
        /// <param name="memberName">Automatically filled in by CallerMemberName.</param>
        /// <param name="fileName">Automatically filled in by CallerFilePath.</param>
        /// <param name="lineNumber">Automatically filled in by CallerLineNumber.</param>
        /// <exception cref="SharperHacks.Diagnostics.VerifyException">
        /// Thrown when <paramref name="expressionResult"/> is not false.
        /// </exception>
        public static void IsFalse(
            bool expressionResult, 
            string comment = null,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string fileName = "",
            [CallerLineNumber] int lineNumber = 0
            )
        {
            if (expressionResult)
            {
                throw new VerifyException(
                    BuildExceptionMessage(
                        ExpressionNotFalseExceptionPrefix, 
                        comment,
                        memberName,
                        fileName,
                        lineNumber
                        ));
            }
        }

        #region Private

        private static string BuildExceptionMessage(
            string prefix, 
            string postfix,
            string memberName,
            string fileName,
            int lineNumber)
        {
            if (!string.IsNullOrEmpty(postfix))
            {
                
                return $"{prefix} {postfix??string.Empty} @ {memberName} [{fileName}({lineNumber})]";
            }

            return $"{prefix} @ {memberName} [{fileName}({lineNumber})]";
        }

        #endregion Private
    }
}
