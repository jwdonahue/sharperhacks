﻿// Copyright (c) Joseph W Donahue dba SharperHacks.org
// https://www.sharperhacks.org
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Runtime.Serialization;

// Deprecated.  Use SharperHacks.Diagnostics instead.

// ReSharper disable once CheckNamespace
namespace SharperHacks.Diagnostics
{
    /// <summary>
    /// The static members of the Verify class will throw one of these when the specified constraint is not met.
    /// </summary>
    [Serializable]
    public class VerifyException : Exception
    {
        /// <summary>
        /// Same as base class.
        /// </summary>
        public VerifyException() : base() {}

        /// <summary>
        /// Same as base class.
        /// </summary>
        /// <param name="message">Same as base class.</param>
        public VerifyException(string message) : base(message) {}

        /// <summary>
        /// Same as base class.
        /// </summary>
        /// <param name="message">Same as base class.</param>
        /// <param name="innerException">Same as base class.</param>
        public VerifyException(string message, Exception innerException) : base(message, innerException) {}

        /// <summary>
        /// Same as base class.
        /// </summary>
        /// <param name="info">Same as base class</param>
        /// <param name="context">Same as base class</param>
        protected VerifyException(SerializationInfo info, StreamingContext context) : base(info, context) {}
    }
}
