SharperHacks.Core2.2 is an Open Source collection of code targetting .Net Core 2.2+.

See LICENSE.txt for the MIT license, copyright Joseph W Donahue (DBA SharperHacks LLC).

To contribute, fork the repo, create a branch, make your changes, then submit a PR.
