@setlocal EnableExtensions

@set _Solution=SharperHacks.Core2.2.sln
@set _OldPrompt=%prompt%
@set prompt=$_$G

pushd %~dp0

@if /i "%1" equ "clean-only" (call :CleanOnly & goto :CleanUpAndExit)

@if /i "%1" equ "clean" (
    @call :DotNet %*
    @if %ErrorLevel% neq 0 (@call :HandleError restore %ErrorLevel% & goto :CleanUpAndExit)
)

@call :DotNet restore
@if %ErrorLevel% neq 0 (@call :HandleError restore %ErrorLevel% & goto :CleanUpAndExit)

@call :DotNet build --no-restore --configuration Release
@if %ErrorLevel% neq 0 (@call :HandleError build %ErrorLevel% & goto :CleanUpAndExit)

@call :DotNet test --no-restore --no-build
@if %ErrorLevel% neq 0 (@call :HandleError test %ErrorLevel% & goto :CleanUpAndExit)

@goto :CleanUpAndExit

:CleanOnly
@shift
@call :DotNet clean %2 %3 %4 %5 %6 %7 %8 %9
@if %ErrorLevel% neq 0 (@call :HandleError restore %ErrorLevel%)
@rd /s /q .BuildOutput\Debug > NUL 2>>&1
@rd /s /q .BuildOutput\Release > NUL 2>>&1
@exit /b

:CleanUpAndExit
@popd
@exit /b

:DotNet
DotNet %* "%_Solution%"
@exit /b

:HandleError
@echo DotNet %1 failed with error code: %2
@exit /b %2
