﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;

namespace SharperHacks
{   
    namespace Options
    {
        /// <summary>
        /// Represents a program/module option.
        /// </summary>
        /// <typeparam name="T">The parameter type.</typeparam>
        /// <remarks>
        /// This class is intended only to be inherited by Option&gt;&lt; and
        /// VerbOption&gt;%lt;, which themselves are sealed.
        /// </remarks>
        public abstract class OptionBase<T> : IOption<T>
        {
            #region IOption<T>

            /// <inheritdoc cref="IOption{T}.DefaultValue"/>
            public T DefaultValue { get; }

            ///<inheritdoc cref="IOption{T}.Value"/>
            public T Value { get; set; }

            #endregion IOption<T>

            #region IOption
#if false
            /// <inheritcod cref="IOption.GetOptionType"/>
            public Type GetOptionType() { return typeof(T); }
#endif
            /// <inheritdoc cref="IOption.IsOptional"/>
            public bool IsOptional { get; protected set; }

            /// <inheritdoc cref="IOption.Name"/>
            public string Name { get; protected set; }

            /// <inheritdoc cref="IOption.Aliases"/>
            public IEnumerable<string> Aliases { get; protected set; }

            /// <inheritdoc cref="IOption.ValueString"/>
            public string ValueString
            {
                get
                {
                    string result = null == Value ? "<null>" : Value.ToString();
                    return result;
                }
            }

            /// <inheritdoc cref="IOption.Usage"/>
            public abstract IUsage Usage { get; protected set; }

            /// <inheritdoc cref="IOption.SetValue"/>
            public bool SetValue(string valueStr)
            {
                try
                {
                    // TODO: Add support of +/- and ++/-- for the appropriate Value types!
                    Value = valueStr.ChangeType<T>();
                }
                catch (Exception)
                {
                    return false;
                }

                return true;
            }
            #endregion IOption

            #region Public
//            public static implicit operator T(OptionBase<T> v) => v.Value;
            #endregion

            #region Protected

            /// <summary>
            /// Override to provide the correct OptionsDB registration behavior.
            /// </summary>
            /// <returns>True on success, false otherwise</returns>
            protected abstract bool RegisterSelf();

            #region Constructors

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="isOptional"></param>
            /// <param name="name"></param>
            protected OptionBase(bool isOptional, string name)
            {
                Verify.IsNotNullOrEmpty(name);
                IsOptional = isOptional;
                Name = name;
                Usage = null;
                Aliases = Enumerable.Empty<string>();
                Verify.IsTrue(RegisterSelf());
            }
#if false
            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="usage"></param>
            protected OptionBase(bool isOptional, string name)
            {
                Verify.IsNotNullOrEmpty(name);
                IsOptional = isOptional;
                Name = name;
                Aliases = Enumerable.Empty<string>();
                Verify.IsTrue(RegisterSelf());
            }
#endif
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="defaultValue"></param>
            protected OptionBase(bool isOptional, string name, T defaultValue)
            {
                Verify.IsNotNullOrEmpty(name);
                IsOptional = isOptional;
                Name = name;
                Value = DefaultValue = defaultValue;
                Aliases = Enumerable.Empty<string>();
                Verify.IsTrue(RegisterSelf());
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="aliases"></param>
            protected OptionBase(bool isOptional, string name, string[] aliases)
            {
                Verify.IsNotNullOrEmpty(name);
                IsOptional = isOptional;
                Name = name;
                Aliases = aliases ?? Enumerable.Empty<string>();
                Verify.IsTrue(RegisterSelf());
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="aliases"></param>
            /// <param name="defaultValue"></param>
            protected OptionBase(bool isOptional, string name, string[] aliases, T defaultValue)
            {
                Verify.IsNotNullOrEmpty(name);
                IsOptional = isOptional;
                Name = name;
                Aliases = aliases ?? Enumerable.Empty<string>();
                Value = DefaultValue = defaultValue;
                Verify.IsTrue(RegisterSelf());
            }
            #endregion

            #endregion Protected

        }
    }
}