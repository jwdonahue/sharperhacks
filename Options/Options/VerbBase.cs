﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;
using System.Collections.Generic;
using System.Reflection;

namespace SharperHacks
{
    namespace Options
    {
        /// <summary>
        /// TODO:
        /// </summary>
        public abstract class VerbBase : IVerb
        {
            // TODO: Add create clone feature for multiple verbs on command line with their own set of option values!

            #region IVerb

            /// <inheritdoc cref="IVerb.IsSingleton"/>
            public bool IsSingleton { get; protected set; } = true;

            /// <inheritdoc cref="IVerb.Name"/>
            public string Name { get; }

            /// <inheritdoc cref="IVerb.Usage"/>
            public IUsage Usage { get; }

            /// <inheritdoc cref="IVerb.Options"/>
            public IEnumerable<IOption> Options => _options;

            /// <inheritdoc cref="IVerb.GetOption"/>
            public IOption GetOption(string optionName)
            {
                IOption result = null;
                foreach (var item in Options)
                {
                    if (item.Name == optionName)
                    {
                        result = item;
                    }
                }

                return result;
            }

            /// <inheritdoc cref="IVerb.Run"/>
//            public virtual int Run() => throw new NotImplementedException();
            public abstract int Run();

            /// <inheritdoc cref="IVerb.GetOption"/>
            /// <remarks>
            /// <para>
            /// Override this for verbs that should execute once for each occurrence (IsSingleton == false).
            /// When this method returns null, the parser will simply reinitialize that verb's options,
            /// when it finds multiple occurrences on the command line.
            /// </para>
            /// </remarks>
            public virtual IVerb Spawn()
            {
                Verify.IsTrue(IsSingleton, "You must override VerbBase.Spawn() when VerbBase.IsSingleton == false.");
                return null;
            }

            #endregion IVerb

            #region Constructors

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="name"></param>
            /// <param name="usage"></param>
            protected VerbBase(string name, IUsage usage)
            {
                Name = name;
                Usage = usage;
                InitializeOptionsProperty();
                OptionsDb.GetTheOptionsDb().AddVerb(this);
            }
            #endregion Constructors

            #region Private

            private List<IOption> _options = new List<IOption>();

            private void InitializeOptionsProperty()
            {
                _options = new List<IOption>();

                var myType = this.GetType();
                var fieldInfo = myType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance
                                                                        | BindingFlags.Public);
                foreach (var item in fieldInfo)
                {
                    // SharperHacks.Options.Option can't be members of VerbBase derived classes.
                    Verify.IsFalse(item.FieldType.AssemblyQualifiedName.StartsWith("SharperHacks.Options.Option"));

                    if (item.FieldType.AssemblyQualifiedName.StartsWith("SharperHacks.Options.VerbOption"))
                    {
                        _options.Add((IOption)item.GetValue(item.IsStatic ? null : this));
                    }
                }
            }
            #endregion Private
        }
    }
}