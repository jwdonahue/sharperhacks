﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;

namespace SharperHacks
{
    namespace Options
    {
        /// <summary>
        /// Usage class implements IUsage and interfaces with OptionsDb, IOption and IVerb,
        /// to provide flexible help/usage infrastructure to program developers.
        /// </summary>
        public class Usage : IUsage
        {
            #region IUsage

            /// <inheritdoc cref="IUsage.KeyScopePair"/>
            public UsageKeyScopePair KeyScopePair { get; }

            /// <inheritdoc cref="IUsage.IsReadMe"/>
            public bool IsReadMe
            {
                get
                {
                    return (KeyScopePair.Scope == UsageDocumentScope.ApplicationReadMe)
                           || (KeyScopePair.Scope == UsageDocumentScope.VerbReadMe)
                           || (KeyScopePair.Scope == UsageDocumentScope.OptionReadMe);
                }
            }
            /// <inheritdoc cref="IUsage.ShortDescription"/>
            public string ShortDescription
            {
                get => _shortDescription ?? string.Empty;
            }

            /// <inheritdoc cref="IUsage.LongDescription"/>
            public IEnumerable<string> LongDescription
            {
                get => _longDescription ?? new [] {string.Empty};
            }

            /// <inheritdoc cref="IUsage.Examples"/>
            public IEnumerable<string> Examples
            {
                get => _examples ?? new [] {string.Empty};
            }

            /// <inheritdoc cref="IUsage.All"/>
            public IEnumerable<string> All
            {
                get
                {
                    if (IsReadMe)
                    {
                        LoadReadMeFile();
                    }
                    else
                    {
                        EnumerateAll();
                    }

                    return _all;
                }
            }

            /// <inheritdoc cref="IUsage.Write(TextWriter)"/>
            public void Write(TextWriter tw)
            {
                foreach(var line in All)
                {
                    tw.WriteLine(line);
                }
            }

            #endregion IUsage

            #region Constructors

            // TODO: Constructors taking file path/name and/or JSON or XML document types.

#if false
            public Usage(string key, IUsageDocument document)
            {
                Verify.IsNotNullOrEmpty(key);
                Verify.IsNotNull(document);

                _key = key;
                _usageDocument = document;
            }
#endif
            /// <summary>
            /// Constructs a Usage object using a simple algorithm to detect
            /// the readme file associated with the current program.
            /// </summary>
            public Usage()
            {
                _fileInfo = GetReadMeFileInfo();

                string keyName = null == _fileInfo ? string.Empty : _fileInfo.Name;
                KeyScopePair = new UsageKeyScopePair(keyName, UsageDocumentScope.ApplicationReadMe);
            }

            /// <summary>
            /// Constructs a Usage object from the supplied keyScopePair.
            /// </summary>
            /// <param name="keyScopePair"></param>
            public Usage(UsageKeyScopePair keyScopePair)
            {
                Verify.IsNotNull(keyScopePair);
                KeyScopePair = keyScopePair;
            }


            /// <summary>
            /// Constructs a Usage object with a copy of the supplied FileInfo.
            /// </summary>
            /// <param name="fileInfo"></param>
            /// <param name="keyScopePair"></param>
            public Usage(FileInfo fileInfo, UsageKeyScopePair keyScopePair)
            {
                Verify.IsNotNull(fileInfo);

                if (fileInfo.Exists)
                {
                    _fileInfo = fileInfo;
                }

                KeyScopePair = keyScopePair;
            }

            /// <summary>
            /// Constructs a Usage object with the supplied shortDescription.
            /// LongDescription and Examples will be set to empty arrays (string[0]).
            /// </summary>
            /// <param name="shortDescription">A short, preferably one line description.</param>
            /// <param name="keyScopePair"></param>
            public Usage(string shortDescription, UsageKeyScopePair keyScopePair)
            {
                Verify.IsNotNull(shortDescription);

                _shortDescription = shortDescription;
                _longDescription = new string[0];
                _examples = new string[0];
                KeyScopePair = keyScopePair;
            }

            /// <summary>
            /// Constructs a Usage object with the supplied short and long descriptions.
            /// </summary>
            /// <param name="shortDescription">A short, preferably one line description.</param>
            /// <param name="longDescription">One paragraph/line (string) description</param>
            /// <param name="keyScopePair"></param>
            public Usage(
                string shortDescription, 
                string[] longDescription, 
                UsageKeyScopePair keyScopePair)
            {
                Verify.IsNotNull(shortDescription);

                _shortDescription = shortDescription;
                _longDescription = longDescription ?? new string[0];
                _examples = new string[0];
                KeyScopePair = keyScopePair;
            }

            /// <summary>
            /// Constructs a Usage object with the supplied short/long descriptions and examples.
            /// </summary>
            /// <param name="shortDescription">A short, preferably one line description.</param>
            /// <param name="longDescription">One paragraph/line (string) description</param>
            /// <param name="examples">One or more example.</param>
            /// <param name="keyScopePair"></param>
            public Usage(string shortDescription, string[] longDescription, string[] examples, UsageKeyScopePair keyScopePair)
            {
                Verify.IsNotNull(shortDescription);

                _shortDescription = shortDescription;
                _longDescription = longDescription ?? new string[0];
                _examples = examples ?? new string[0];
                KeyScopePair = keyScopePair;
            }
            #endregion Constructors

            #region public statics

            /// <summary>
            /// Uses a simple search algorithm to find the ReadMe file.
            /// </summary>
            /// <returns>FileInfo on success or null if none were found.</returns>
            public static FileInfo GetReadMeFileInfo()
            {
                string programPath = AppDomain.CurrentDomain.BaseDirectory;
                string programName = AppDomain.CurrentDomain.FriendlyName;

                // First try programPath\ProgramName.ReadMe
                string fqpn = Path.Combine(programPath, programName) + ".ReadMe";

                if (File.Exists(fqpn))
                {
                    return new FileInfo(fqpn);
                }

                // Try programPath\ProgramName.ReadMe.txt
                fqpn += ".txt";
                if (File.Exists(fqpn))
                {
                    return new FileInfo(fqpn);
                }

                // Try programPath\ReadMe
                fqpn = Path.Combine(programPath, "ReadMe");
                if (File.Exists(fqpn))
                {
                    return new FileInfo(fqpn);
                }

                // Try programPath\ReadMe.txt
                fqpn += ".txt";
                if (File.Exists(fqpn))
                {
                    return new FileInfo(fqpn);
                }

                return null;
            }

            /// <summary>
            /// Write usage content to Console.out.
            /// </summary>
            /// <param name="programUsage"></param>
            public static void Write(IUsage programUsage) => Write(Console.Out, programUsage);

            /// <summary>
            /// Write usage content to TextWriter
            /// </summary>
            /// <param name="tw"></param>
            /// <param name="programUsage"></param>
            public static void Write(TextWriter tw, IUsage programUsage)
            {
                tw.WriteLine(programUsage.ShortDescription);
                tw.WriteLine("");

                Write(tw, programUsage.LongDescription);
                Write(tw, programUsage.Examples);

                OptionsDb optionsDb = OptionsDb.GetTheOptionsDb();
                WriteOptions(tw, optionsDb.NonVerbOptions);
                WriteVerbs(tw, optionsDb.Verbs);
            }

            #endregion public statics

            #region Private
#if false
            private static object _classLock = new object();

            private List<IUsage> _UsageDb = new List<IUsage>(25);
#endif
            private readonly string _shortDescription;
            private readonly string[] _longDescription;
            private readonly string[] _examples;

            private List<string> _all = null;

            private readonly FileInfo _fileInfo;

            private void EnumerateAll()
            {
                if (null == _all)
                {
                    _all = new List<string>
                    {
                        _shortDescription ?? string.Empty
                    };
                }
                else
                {
                    return;
                }

                foreach (var line in LongDescription)
                {
                    _all.Add(line);
                }

                foreach (var line in Examples)
                {
                    _all.Add(line);
                }
            }

            private void LoadReadMeFile()
            {
                if (_fileInfo == null)
                {
                    _all = new List<string>();
                    return;
                }
                
                _all = new List<string>(File.ReadAllLines(_fileInfo.FullName));
            }

            private static void Write(TextWriter tw, IEnumerable<string> strings)
            {
                bool any = false;

                foreach (string line in strings)
                {
                    any = true;
                    tw.WriteLine(line);
                }

                if (any)
                {
                    tw.WriteLine("");
                }
            }

            private static void WriteOptions(TextWriter tw, IEnumerable<IOption> options)
            {
                foreach (var option in options)
                {
                    if (null != option.Usage)
                    {
                        WriteOptionHeader(tw, option);
                        tw.WriteLine(option.Usage.ShortDescription);
                        tw.WriteLine("");
                        Write(tw, option.Usage.LongDescription);
                        Write(tw, option.Usage.Examples);
                    }
                }
            }

            private static void WriteOptionHeader(TextWriter tw, IOption option)
            {
                Verify.IsNotNullOrEmpty(option.Name);
                StringBuilder sb = new StringBuilder(option.Name);

                if (null != option.Aliases && option.Aliases.Any())
                {
                    sb.Append(" (AKA:");
                    foreach (var alias in option.Aliases)
                    {
                        sb.Append(' ');
                        sb.Append(alias);
                    }

                    sb.Append(')');
                }

                sb.Append(':');
                tw.WriteLine(sb.ToString());
            }

            private static void WriteVerbs(TextWriter tw, IEnumerable<IVerb> verbs)
            {
                foreach (var verb in verbs)
                {
                    tw.WriteLine("Verb: " + verb.Name);
                    tw.WriteLine("");
                    tw.WriteLine("\t" + verb.Usage.ShortDescription);
                    tw.WriteLine("");
                    Write(tw, verb.Usage.Examples);
                    tw.WriteLine("");
                    WriteOptions(tw, verb.Options);
                    tw.WriteLine("");
                    Write(tw, verb.Usage.LongDescription);
                }
            }

            #endregion Private
        }

    }
}