﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Options.Interfaces;

namespace SharperHacks
{
    namespace Options
    {
        /// <summary>
        /// Extends OptionBase, providing appropriate RegisterSelf() behavior.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public sealed class VerbOption<T> : OptionBase<T>
        {
            #region IOption

            /// <inheritdoc />
            public override IUsage Usage { get; protected set; }
            #endregion

            #region Constructors

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="isOptional"></param>
            /// <param name="name"></param>
            public VerbOption(bool isOptional, string name) : base(isOptional, name)
            {
                Usage = new Usage(new UsageKeyScopePair(name, UsageDocumentScope.VerbOptionReadMe));
            }

            /// <summary>
            /// Constructor.
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="usage"></param>
            public VerbOption(bool isOptional, string name, IUsage usage) : base(isOptional, name)
            {
                Usage = usage;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="defaultValue"></param>
            /// <param name="usage"></param>
            public VerbOption(bool isOptional, string name, T defaultValue, IUsage usage)
                : base(isOptional, name, defaultValue)
            {
                Usage = usage;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="aliases"></param>
            /// <param name="usage"></param>
            public VerbOption(bool isOptional, string name, string[] aliases, IUsage usage)
                : base(isOptional, name, aliases)
            {
                Usage = usage;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="isOptional">
            /// Determines whether the option must be specified.
            /// </param>
            /// <param name="name">
            /// The option name. Must not be null or empty.
            /// </param>
            /// <param name="aliases"></param>
            /// <param name="defaultValue"></param>
            /// <param name="usage"></param>
            public VerbOption(bool isOptional, string name, string[] aliases, T defaultValue, IUsage usage)
                : base(isOptional, name, aliases, defaultValue)
            {
                Usage = usage;
            }

            #endregion Constructors

            #region Protected

            /// <summary>
            /// Registers as non-verb option.
            /// </summary>
            /// <returns></returns>
            /// <remarks>
            /// The verb object tracks its own options.
            /// This allows multiple verbs or instances thereof, to
            /// have identical option names, with different value states.
            /// </remarks>
            protected override bool RegisterSelf()
            {
                // The verb object tracks its own options.
                // This allows multiple verbs to have identical option names,
                // with different value states.
                return true; 
            }

            #endregion Protected

        }
    }
}