﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Options.Interfaces;
using System;
using System.IO;

namespace SharperHacks.Options
{
    /// <summary>
    /// Helper class provides type conversion.
    /// </summary>
    internal static class Converter
    {
        public static T ChangeType<T>(this object obj)
        {
            return (T)Convert.ChangeType(obj, typeof(T));
        }
    }

    /// <summary>
    /// Extends OptionBase&lt;&gt;, providing appropriate RegisterSelf() behavior.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class Option<T> : OptionBase<T>
    {
        #region IOption

        /// <inheritdoc />
        public override IUsage Usage { get; protected set; }
        #endregion

        #region Factories

        /// <summary>
        /// OptionT factory.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="aliases"></param>
        /// <param name="isOptional"></param>
        /// <param name="defaultValue"></param>
        /// <param name="readMeFile"></param>
        /// <returns></returns>
        public static Option<T> CreateOption(
            string name,
            string[] aliases,
            bool isOptional = true,
            T defaultValue = default(T),
            FileInfo readMeFile = null
            )
        {
            var usage = 
                null == readMeFile 
                ? new Usage() 
                : new Usage(readMeFile, new UsageKeyScopePair(name, UsageDocumentScope.OptionReadMe));


            var option = new Option<T>(
                isOptional,
                name,
                aliases,
                defaultValue,
                usage
                );

            return option;
        }

        /// <summary>
        /// OptionT factory.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="aliases"></param>
        /// <param name="isOptional"></param>
        /// <param name="shortDescription"></param>
        /// <param name="longDescription"></param>
        /// <param name="examples"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static Option<T> CreateOption(
            string name,
            string[] aliases,
            bool isOptional,
            string shortDescription,
            string[] longDescription = null,
            string[] examples = null,
            T defaultValue = default(T)
            )
        {
            var ksp = new UsageKeyScopePair(name, UsageDocumentScope.OptionSource);
            
            var usage = new Usage(
                shortDescription,
                longDescription,
                examples,
                ksp
                );

            var option = new Option<T>(
                isOptional,
                name,
                aliases,
                defaultValue,
                usage
            );

            return option;
        }

        #endregion Factories

        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="isOptional"></param>
        /// <param name="name"></param>
        public Option(bool isOptional, string name) : base(isOptional, name)
        {
            Usage = new Usage(new UsageKeyScopePair(name, UsageDocumentScope.OptionReadMe));
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="isOptional">
        /// Determines whether the option must be specified.
        /// </param>
        /// <param name="name">
        /// The option name. Must not be null or empty.
        /// </param>
        /// <param name="usage"></param>
        public Option(bool isOptional, string name, IUsage usage) : base(isOptional, name)
        {
            Usage = usage;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="isOptional">
        /// Determines whether the option must be specified.
        /// </param>
        /// <param name="name">
        /// The option name. Must not be null or empty.
        /// </param>
        /// <param name="defaultValue"></param>
        /// <param name="usage"></param>
        public Option(bool isOptional, string name, T defaultValue, IUsage usage)
            : base(isOptional, name, defaultValue)
        {
            Usage = usage;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="isOptional">
        /// Determines whether the option must be specified.
        /// </param>
        /// <param name="name">
        /// The option name. Must not be null or empty.
        /// </param>
        /// <param name="aliases"></param>
        /// <param name="usage"></param>
        public Option(bool isOptional, string name, string[] aliases, IUsage usage)
            : base(isOptional, name, aliases)
        {
            Usage = usage;
        }


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="isOptional">
        /// Determines whether the option must be specified.
        /// </param>
        /// <param name="name">
        /// The option name. Must not be null or empty.
        /// </param>
        /// <param name="aliases"></param>
        /// <param name="defaultValue"></param>
        /// <param name="usage"></param>
        public Option(bool isOptional, string name, string[] aliases, T defaultValue, IUsage usage)
            : base(isOptional, name, aliases, defaultValue)
        {
            Usage = usage;
        }


        #endregion Constructors

        #region Protected

        /// <summary>
        /// Registers as non-verb option.
        /// </summary>
        /// <returns>True on success, false otherwise (the option already exists)</returns>
        protected override bool RegisterSelf() => OptionsDb.GetTheOptionsDb().AddNonVerbOption(this);

        #endregion Protected
    }
}