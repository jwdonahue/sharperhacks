﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Options.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SharperHacks
{
    namespace Options
    {
        /// <summary>
        /// Encapsulates unmatched and parse error handling.
        /// </summary>
        public sealed class UsageErrorHandler : IUsageErrorHandler
        {
            #region Public

            /// <inheritdoc cref="IUsageErrorHandler.HasFailed"/>
            public bool HasFailed => HasUnmatched || HasParseErrors;

            /// <inheritdoc cref="IUsageErrorHandler.HasUnmatched"/>
            public bool HasUnmatched => Unmatched.Any();

            /// <inheritdoc cref="IUsageErrorHandler.HasParseErrors"/>
            public bool HasParseErrors => ParseErrors.Any();

            /// <inheritdoc cref="IUsageErrorHandler.AddUnmatched"/>
            public void AddUnmatched(string item)
            {
                lock (_classAccessLock)
                {
                    _unmatched.Add(item);
                }
            }

            /// <inheritdoc cref="IUsageErrorHandler.AddError"/>
            public void AddError(string item)
            {
                lock (_classAccessLock)
                {
                    _parseErrors.Add(item);
                }
            }

            /// <summary>
            /// Default error handler writes parse errors and unmatched
            /// arguments to the specified output.
            /// </summary>
            /// <param name="unmatched"></param>
            /// <param name="errors"></param>
            /// <param name="output"></param>
            public void DefaultHandler(IEnumerable<string> unmatched,
                IEnumerable<string> errors,
                TextWriter output)
            {
                lock (_classAccessLock)
                {
                    WriteUnmatched(unmatched, output);
                    WriteErrors(errors, output);

                    // This might be just the application summary or the entire
                    // application readme.
                    _usage.Write(Console.Out);

                    WriteVerbAndOptionUsageIfAny(output);
                }
            }

            private void WriteErrors(IEnumerable<string> errors, TextWriter output)
            {
                if (errors.Any())
                {
                    output.WriteLine("Errors:");
                    foreach (var item in errors)
                    {
                        output.WriteLine("  " + item);
                    }

                    output.WriteLine();
                }
            }

            private void WriteUnmatched(IEnumerable<string> unmatched, TextWriter output)
            {
                if (unmatched.Any())
                {
                    output.WriteLine("Failed to process args:");
                    foreach (var item in unmatched)
                    {
                        output.WriteLine("  " + item);
                    }

                    output.WriteLine();
                }
            }

            private void WriteVerbAndOptionUsageIfAny(TextWriter output)
            {
                var optionsDb = OptionsDb.GetTheOptionsDb();

                foreach (var option in optionsDb.NonVerbOptions)
                {
                    option.Usage.Write(output);
                }

                output.WriteLine();

                foreach (var verb in optionsDb.Verbs)
                {
                    verb.Usage.Write(output);
                    foreach (var verbOption in verb.Options)
                    {
                        verbOption.Usage.Write(output);
                    }
                }

                output.WriteLine();
            }

            /// <inheritdoc cref="IUsageErrorHandler.Unmatched"/>
            public IEnumerable<string> Unmatched
            {
                get
                {
                    lock (_classAccessLock)
                    {
                        foreach (var item in _unmatched)
                        {
                            yield return item;
                        }
                    }
                }
            }

            /// <inheritdoc cref="IUsageErrorHandler.ParseErrors"/>
            public IEnumerable<string> ParseErrors
            {
                get
                {
                    lock (_classAccessLock)
                    {
                        return _parseErrors.ToArray();
                    }
                }
            }

            /// <inheritdoc cref="IUsageErrorHandler.Invoke()"/>
            public void Invoke()
            {
                Invoke(Console.Out);
            }

            ///<inheritdoc cref="IUsageErrorHandler.Invoke(TextWriter)"/>
            public void Invoke(TextWriter tw)
            {
                if (null == _usage)
                {
                    _usage = new Usage();
                }

                _errorHandler(_unmatched, _parseErrors, tw);
            }

            /// <summary>
            /// Resets UsageErrorHandler state.  Primarily used by unit tests.
            /// </summary>
            public void Reset()
            {
                lock (_classAccessLock)
                {
                    _unmatched = new List<string>();
                    _parseErrors = new List<string>();
                }
            }

            #region Constructors
            /// <summary>
            /// Default constructor uses DefaultHandler
            /// </summary>
            public UsageErrorHandler()
            {
                _errorHandler = DefaultHandler;
            }

            /// <summary>
            /// Constructor for caller provided Usage and UsageErrorHandler.
            /// </summary>
            /// <param name="appUsage"></param>
            /// <param name="handler"></param>
            public UsageErrorHandler(Usage appUsage, UsageErrorHandlerDelegate handler)
            {
                _usage = appUsage;
                _errorHandler = handler ?? DefaultHandler;
            }
#if false
            /// <summary>
            /// Constructor for caller provided error handler.B
            /// </summary>
            /// <param name="handler"></param>
            public UsageErrorHandler(UsageErrorHandlerDelegate handler)
            {
                _errorHandler = handler;
            }
#endif
            #endregion Constructors

            #endregion Public

            #region Private

            private static readonly object _classAccessLock = new object();

            private Usage _usage = null;

            private readonly UsageErrorHandlerDelegate _errorHandler;

            // Accumulates parse error messages as arguments are processed.
            private List<string> _parseErrors = new List<string>();

            // Holds parsed arguments that don't match any registered verbs or options.
            private List<string> _unmatched = new List<string>();

            #endregion Private
        }
    }
}