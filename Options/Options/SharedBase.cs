﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.IO;
using System.Linq;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options
{
    /// <summary>
    /// Common code inherited by OptionsBase and VerbBase.
    /// </summary>
    /// <remarks>
    /// <para>All constructors are internal.</para>
    /// <para>Default constructor will throw NotImplementedException.</para>
    /// </remarks>
    public abstract class SharedBase
    {
        #region Public

        /// <summary>
        /// Get whether there were any unmatched segments in arguments.
        /// </summary>
        public bool HasUnmatched
        {
            get => Parser.UsageErrorHandler.Unmatched.Any();
        }

        /// <summary>
        /// Get whether there were any parse errors.
        /// </summary>
        public bool HasParseErrors
        {
            get => Parser.UsageErrorHandler.ParseErrors.Any();
        }

        /// <summary>
        /// The OptionsParser instance used to parse options and verbs.
        /// </summary>
        public OptionsParser Parser { get; set; }

        /// <summary>
        /// This is overriden by OptionsBase and VerbsBase.
        /// </summary>
        /// <remarks>
        /// Can be overriden by OptionsBase and VerbsBase 
        /// </remarks>
        public abstract int RunIfParsedSuccessfully();

        #endregion Public

        #region Protected

        /// <summary>
        /// The UsageErrorHandler to invoke on parser errors.
        /// </summary>
        protected IUsageErrorHandler ErrorHandler;

        // TODO: Add constructor that takes an initialized OptionsParser (IOptionsParser).

        #endregion Protected

        #region Internal

        internal SharedBase(
            string[] args,
            IUsageErrorHandler errorHandler,
            string[] prefixes,
            string[] binders,
            FileInfo usageFile,
            UsageDocumentScope scope,
            bool supressErrorHandlerInvoke = false)
        {
            Initialize(args, errorHandler, false, prefixes, binders, usageFile, scope, supressErrorHandlerInvoke);
        }

        internal SharedBase(
            string[] args,
            IUsageErrorHandler errorHandler,
            bool requiresVerb,
            string[] prefixes,
            string[] binders,
            FileInfo usageFile,
            UsageDocumentScope scope,
            bool supressErrorHandlerInvoke = false)
        {
            Initialize(args, errorHandler, requiresVerb, prefixes, binders, usageFile, scope, supressErrorHandlerInvoke);
        }
        #endregion

        #region Private

        private void Initialize(
            string[] args,
            IUsageErrorHandler errorHandler,
            bool requiresVerb,
            string[] prefixes,
            string[] binders,
            FileInfo usageFile,
            UsageDocumentScope scope,
            bool supressErrorHandlerInvoke = false)
        {
            ErrorHandler = errorHandler;

            Parser = new OptionsParser(errorHandler, prefixes, binders)
            {
                RequiresVerb = requiresVerb
            };

            if (!Parser.UpdateOptionsFromArguments(args) && !supressErrorHandlerInvoke)
            {
                ErrorHandler.Invoke();
            }
        }

        #endregion Private
    }
}