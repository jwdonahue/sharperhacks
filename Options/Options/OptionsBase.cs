﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.IO;
using System.Linq;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options
{
    /// <summary>
    /// OptionsBase is a helper class.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Inherit from this class to contain your non-verb program options.
    /// </para>
    /// </remarks>
    public abstract class OptionsBase : SharedBase
    {
        #region Public
        /// <summary>
        /// Calls Run() if there are no parse errors or unmatched options.
        /// </summary>
        /// <returns>Status returned by Run().</returns>
        public override int RunIfParsedSuccessfully()
        {
            if (HasUnmatched || HasParseErrors) return -1; // TODO: Better error codes!
            return Run();
        }

        #endregion

        #region Constructors

        // TODO: Need constructor for source defined usage.

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="errorHandler"></param>
        protected OptionsBase(string[] args, UsageErrorHandler errorHandler) : 
            base(
                args, 
                errorHandler, 
                OptionsParser.DefaultPrefixes.ToArray(), 
                OptionsParser.DefaultBinders.ToArray(),
                null,
                UsageDocumentScope.ApplicationReadMe
            )
        {}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="errorHandler"></param>
        /// <param name="prefixes"></param>
        /// <param name="binders"></param>
        protected OptionsBase(
            string[] args,
            UsageErrorHandler errorHandler,
            string[] prefixes,
            string[] binders
            ) : 
                base(
                    args,
                    errorHandler, 
                    prefixes, 
                    binders,
                    null,
                    UsageDocumentScope.ApplicationReadMe
                )
        { }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="errorHandler"></param>
        /// <param name="prefixes"></param>
        /// <param name="binders"></param>
        /// <param name="usageFile"></param>
        protected OptionsBase(
            string[] args,
            UsageErrorHandler errorHandler,
            string[] prefixes,
            string[] binders,
            FileInfo usageFile
            ) : 
                base(
                    args,
                    errorHandler, 
                    prefixes, 
                    binders,
                    usageFile,
                    UsageDocumentScope.ApplicationReadMe
                )
        {}

        /// <summary>
        /// Override this Run() method, to define your program behavior.
        /// </summary>
        protected abstract int Run();

        #endregion Constructors
    }
}