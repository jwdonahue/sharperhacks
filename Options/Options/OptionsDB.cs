﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using SharperHacks.Diagnostics;

namespace SharperHacks
{
    namespace Options
    {
        /// <summary>
        /// Container for registered verbs and options.
        /// </summary>
        /// <remarks>
        /// As Options and Verbs are initialized, they register themselves with
        /// the OptionsDb.  This class keeps track of the current state of each
        /// option as config file(s), command line arguments and run-time logic
        /// are processed.
        /// </remarks>
        public sealed class OptionsDb
        {
            #region Private Data
            private readonly Dictionary<int, IOption> _nonVerbOptions = new Dictionary<int, IOption>();
            private readonly List<Tuple<string, IVerb>> _verbs = new List<Tuple<string, IVerb>>();
            private static OptionsDb _theOptionsDb;
            private static readonly object _staticAccessLock = new object();
            #endregion Private Data

            #region Public

            /// <summary>
            /// Get all options not associated with a verb.
            /// </summary>
            public IEnumerable<IOption> NonVerbOptions
            {
                get
                {
                    foreach (var item in _nonVerbOptions)
                    {
                        yield return item.Value;
                    }
                }
            }

            /// <summary>
            /// Get all verbs.
            /// </summary>
            public IEnumerable<IVerb> Verbs
            {
                get
                {
                    foreach (var item in _verbs)
                    {
                        yield return item.Item2;
                    }
                }
            }

            /// <summary>
            /// Add a non-verb option.
            /// </summary>
            /// <param name="option"></param>
            /// <returns></returns>
            /// <remarks>
            /// Options use this interface to register themselves and
            /// their initial state.
            /// </remarks>
            public bool AddNonVerbOption(IOption option)
            {
                Verify.IsNotNull(option);
                Verify.IsNotNullOrEmpty(option.Name);

                return _nonVerbOptions.TryAdd(option.GetHashCode(), option);
            }

            /// <summary>
            /// Adds the specified verb to the database.
            /// </summary>
            /// <param name="verb">The verb to add.</param>
            /// <returns>True on success, false otherwise.</returns>
            /// <Exception cref="VerifyException">
            /// Throws VerifyException if verb is null or verb.Name is null or empty.
            /// </Exception>
            public bool AddVerb(IVerb verb)
            {
                Verify.IsNotNull(verb);
                Verify.IsNotNullOrEmpty(verb.Name);

                if (!_verbs.Exists(x => x.Item1 == verb.Name))
                {
                    _verbs.Add(new Tuple<string, IVerb>(verb.Name, verb));
                    return true;
                }

                return false;
            }

            /// <summary>
            /// Get the specified non-verb option.
            /// </summary>
            /// <param name="name">Option name to search for.</param>
            /// <param name="option">
            /// The option if found, null otherwise.
            /// </param>
            /// <returns>
            /// True on success, false otherwise.
            /// </returns>
            public bool GetNonVerbOption(string name, out IOption option)
            {
                Verify.IsNotNullOrEmpty(name);
                option = null;

                foreach (var item in _nonVerbOptions)
                {
                    if (item.Value.Name == name)
                    {
                        option = item.Value;
                    }

                    if (item.Value.Aliases.Any(alias => alias == name))
                    {
                        option = item.Value;
                    }
                }

                return null != option;
            }

            /// <summary>
            /// Creates a static OptionsDb object, if one hasn't already been created.
            /// </summary>
            /// <returns>The static OptionsDb.</returns>
            /// <remarks>
            /// <para>
            /// The class is not static in order to enable parallel unit testing, but applications
            /// will generally only ever need the one instance.
            /// </para>
            /// </remarks>
            public static OptionsDb GetTheOptionsDb()
            {
                lock (_staticAccessLock)
                {
                    if (null == _theOptionsDb)
                    {
                        _theOptionsDb = new OptionsDb();
                    }
                    Verify.IsNotNull(_theOptionsDb);
                    return _theOptionsDb;
                }
            }

            /// <summary>
            /// We use this for unit tests.
            /// </summary>
            public static void Reset()
            {
                lock (_staticAccessLock)
                {
                    _theOptionsDb = null;
                }
            }

#endregion Public
        }
    }
}