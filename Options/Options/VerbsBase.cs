﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;
using System.IO;
using System.Linq;

namespace SharperHacks.Options
{
    /// <summary>
    /// VerbsBase is a helper class.
    /// </summary>
    /// <remarks>
    /// <para>
    /// Inherit from this class to contain your verbs and non-verb program options.
    /// </para>
    /// <para>
    /// Instantiate one of these if your program only processes verbs.
    /// </para>
    /// </remarks>
    public class VerbsBase : SharedBase
    {
        #region Constructors

        /// <summary>
        /// Constructor for when the default parsing configuration is acceptable.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="errorHandler"></param>
        /// <param name="requiresVerb"></param>
        /// <param name="allowMultipleVerbs"></param>
        /// <param name="continueOnVerbFailure"></param>
        public VerbsBase(
            string[] args, 
            UsageErrorHandler errorHandler, 
            bool requiresVerb, 
            bool allowMultipleVerbs,
            bool continueOnVerbFailure
            ) : base(
                args,
                errorHandler,
                requiresVerb,
                OptionsParser.DefaultPrefixes.ToArray(),
                OptionsParser.DefaultBinders.ToArray(),
                null,
                UsageDocumentScope.ApplicationReadMe,
                true // VerbsBase might add to the list before ErrorHandler.Invoke() is called.
                )
        {
            RequiresVerb = requiresVerb;
            AllowMultipleVerbs = allowMultipleVerbs;
            ContinueOnVerbFailure = continueOnVerbFailure;
        }

        /// <summary>
        /// Constructor for caller to provide parser configuration options.
        /// </summary>
        /// <param name="args"></param>
        /// <param name="errorHandler"></param>
        /// <param name="requiresVerb"></param>
        /// <param name="allowMultipleVerbs"></param>
        /// <param name="continueOnVerbFailure"></param>
        /// <param name="prefixes"></param>
        /// <param name="binders"></param>
        /// <param name="usageFile"></param>
        public VerbsBase(
            string[] args,
            UsageErrorHandler errorHandler,
            bool requiresVerb,
            bool allowMultipleVerbs,
            bool continueOnVerbFailure,
            string[] prefixes,
            string[] binders,
            FileInfo usageFile
        ) : base(
                args,
                errorHandler,
                requiresVerb,
                prefixes,
                binders,
                usageFile,
                UsageDocumentScope.ApplicationReadMe,
                true  // VerbsBase might add to the list before ErrorHandler.Invoke() is called.
            )
        {
            RequiresVerb = requiresVerb;
            AllowMultipleVerbs = allowMultipleVerbs;
            ContinueOnVerbFailure = continueOnVerbFailure;
        }

        #endregion Constructors

        // TODO: Evaluate whether any of this should be moved down to the parser.
        #region Public

        /// <summary>
        /// Get or set the flag indicating whether to allow multiple verbs.
        /// </summary>
        public bool AllowMultipleVerbs { get; set; }

        /// <summary>
        /// Get or set the flag indicating whether to continue after a verb fails.
        /// </summary>
        public bool ContinueOnVerbFailure { get; set; }

        /// <summary>
        /// Get whether any verbs have registered.
        /// </summary>
        public bool HasVerbs => Parser.HasVerbs;

        /// <summary>
        /// Get whether at least one verb is required.
        /// </summary>
        public bool RequiresVerb { get; }

        #endregion Public

        #region Protected

        /// <summary>
        /// Called by RunIfParsedSuccessfully() and there are no verbs to run.
        /// </summary>
        /// <returns>
        /// Should return zero on success or an error code.
        /// </returns>
        /// <remarks>
        /// Override this method if your program should perform a default action.
        /// </remarks>
        protected virtual int WhenNoVerbsFound()
        {
            if (RequiresVerb)
            {
                return -1; // TODO: Better error codes.
            }

            return 0;
        }

        /// <summary>
        /// Defaults to running every verb parsed, if multiple verbs allowed,
        /// otherwise, it will run the one verb found.
        /// </summary>
        /// <remarks>
        /// Provide your own run, if you have special verb handling in mind.
        /// </remarks>
        protected virtual int Run()
        {
            long verbsParsed = Parser.VerbCount;

            // This can fail, if RunIfParsedSuccessfully() was overriden and failed to check
            // the count prior to calling Run().  Coding errors developing this library have 
            // also hit this.
            Verify.IsTrue(Parser.HasVerbs, "RunPreThenVerbsThenPost() should not be called if no verbs parsed.");

            if (!AllowMultipleVerbs && verbsParsed > 1)
            {
                ErrorHandler.AddError("Multiple verbs not allowed, and verbs parsed equals " + verbsParsed.ToString());
                ErrorHandler.Invoke();
                return -1; // TODO: Again, we need better error codes!
            }
            
            return RunPreThenVerbsThenPost();
        }

        /// <summary>
        /// This is the default implementation for Run(), which can be overriden.
        /// This is here so that child classes can override Run() and still invoke
        /// the default behavior.
        /// </summary>
        /// <returns></returns>
        protected int RunPreThenVerbsThenPost()
        {
            int result = 0;

            if (0 == PreVerbsRun())
            {
                result = VerbsRun();
                if (0 == result)
                {
                    result = PostVerbsRun();
                }
            }
            else
            {
                result = -1;  // TODO: Better status return values.
            }

            return result;
        }

        /// <summary>
        /// Calls Run() if there are verbs to run and no parse errors or unmatched options.
        /// </summary>
        /// <returns>Status returned by Run().</returns>
        public override int RunIfParsedSuccessfully()
        {
            int result = 0;

            // If verbs required and none are found, the parser has already flagged
            // it as a parse error.  We test in this order because WhenNoVerbsFound()
            // should be called if none were found, regardless of the accumulated errors
            // from the parser.
            if (!Parser.HasVerbs)
            {
                result = WhenNoVerbsFound();
            }

            if (HasUnmatched || HasParseErrors || !Parser.HasVerbs || result != 0)
            {
                ErrorHandler.Invoke();
                
                return result;
            }

            return Run();
        }

        /// <summary>
        /// Called prior to executing Run() methods of all verbs found on command line.
        /// </summary>
        /// <remarks>
        /// <para>
        /// NOTE: This will not be called if there were no verbs found on command line.
        /// </para>
        /// </remarks>
        /// <returns>0 on success, a status code otherwise.</returns>
        protected virtual int PreVerbsRun() => 0;

        /// <summary>
        /// Called after executing Run() methods of all verbs found on command line.
        /// </summary>
        /// <remarks>
        /// <para>
        /// NOTE: This will not be called if there were no verbs found on command line.
        /// </para>
        /// </remarks>
        /// <returns></returns>
        protected virtual int PostVerbsRun() => 0;
        #endregion Protected

        #region Private
        private int VerbsRun()
        {
            int result = 0;
            foreach (var verb in Parser.VerbsFound)
            {
                result = verb.Run();
                if (result != 0 && !ContinueOnVerbFailure)
                {
                    break;
                }
            }

            return result;
        }
        #endregion Private
    }
}