﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SharperHacks.Options
{
    /// <summary>
    /// OptionsParser class processes program arguments and updates OptionsDB.
    /// </summary>
    public class OptionsParser : IDisposable
    {

        #region Public

        /// <summary>
        /// Default binders used when user does not supply them to the OptionsParser constructor.
        /// </summary>
        public static IEnumerable<string> DefaultBinders
        {
            get => _defaultBinders;
        }

        /// <summary>
        /// Default prefixes used when user does not supply them to the OptionsParser constructor.
        /// </summary>
        public static IEnumerable<string> DefaultPrefixes
        {
            get => _defaultPrefixes;
        }

        /// <summary>
        /// Get the option binder array that is used in parsing command line.
        /// </summary>
        /// <remarks>
        /// Primarily for unit testing purposes.
        /// </remarks>
        public string[] OptionBinders
        {
            get
            {
                lock (_classAccessLock)
                {
                    return _optionBinders;
                }
            }
        }

        /// <summary>
        /// Get the option prefixes that are used in parsing command line.
        /// </summary>
        /// <remarks>
        /// Primarily for unit testing purposes.
        /// </remarks>
        public string[] OptionPrefixes
        {
            get
            {
                lock (_classAccessLock)
                {
                    return _optionPrefixes;
                }
            }
        }

        /// <summary>
        /// Gets or sets whether a verb is required.
        /// </summary>
        public bool RequiresVerb { get; set; }

        /// <summary>
        /// Get or Set the UsageErrorHandler.
        /// </summary>
        public IUsageErrorHandler UsageErrorHandler { get; set; }

        /// <summary>
        /// Get the count of verbs successfully parsed.
        /// </summary>
        public Int64 VerbCount => Interlocked.Read(ref _verbCount);

        /// <summary>
        /// An enumerable list of the verbs found in the args processed up to this point.
        /// </summary>
        public IEnumerable<IVerb> VerbsFound
        {
            get
            {
                lock (_classAccessLock)
                {
                    return new List<IVerb>(_verbsFound);
                }
            }
        }

        /// <summary>
        /// Get whether any verbs have registered.
        /// </summary>
        public bool HasVerbs => VerbsFound.Any();
        
        /// <summary>
        /// Update options from arguments.
        /// </summary>
        /// <param name="args">Arguments to parse.</param>
        /// <returns></returns>
        public bool UpdateOptionsFromArguments(IEnumerable<string> args)
        {
            lock (_classAccessLock)
            {
                bool result = ProcessArgs(args);

                if (RequiresVerb && !HasVerbs)
                {
                    result = false;
                    UsageErrorHandler.AddError("At least one verb is required and none were found on the command line.");
                }

                return result;
            }
        }

        /// <summary>
        /// For testing purposes.
        /// </summary>
        /// <returns></returns>
        public void Reset()
        {
            lock (_classAccessLock)
            {
                _currentVerb = string.Empty;
                RequiresVerb = false;
                UsageErrorHandler.Reset();
            }

        }

        #region Constructors

        /// <summary>
        /// Constructor.  Assigns usage error handler.
        /// </summary>
        /// <param name="usageErrorHandler"></param>
        public OptionsParser(UsageErrorHandler usageErrorHandler)
        {
            UsageErrorHandler = usageErrorHandler;
            _optionBinders = _defaultBinders;
            _optionPrefixes = _defaultPrefixes;
        }

        /// <summary>
        /// Constructor.  Assigns usage error handler, prefix and binder strings.
        /// </summary>
        /// <param name="usageErrorHandler"></param>
        /// <param name="optionPrefixes"></param>
        /// <param name="optionBinders"></param>
        public OptionsParser(
            IUsageErrorHandler usageErrorHandler,
            IEnumerable<string> optionPrefixes,
            IEnumerable<string> optionBinders)
        {
            UsageErrorHandler = usageErrorHandler;
            _optionBinders = optionBinders.ToArray();
            _optionPrefixes = optionPrefixes.ToArray();
        }
        #endregion Constructors

        #endregion Public

        #region Private Data

        // It's possible that multiple threads could be attempting to initialize arguments
        // from multiple asynchronous sources. The lock prevents internal corruption. 
        private readonly object _classAccessLock = new object();

        private string _currentVerb = string.Empty;
        private Int64 _verbCount = 0;
        private List<IVerb> _verbsFound = new List<IVerb>(); // TODO: This belongs in the OptionsDB.

        private static readonly string[] _defaultBinders = new string[] { @" \*", @"\t\*", ":", "=" };
        private static readonly string[] _defaultPrefixes = new string[] { "-", "--", "/" };

        private readonly string[] _optionPrefixes;
        private readonly string[] _optionBinders;

        #endregion PrivateData

        #region Private Methods

        private struct IndexAndLength<T> // TODO: Move this type declaration to a public, reusable location.
        {
            public T Index;
            public T Length;
        }

        private IndexAndLength<int> GetBinderStartAndLength(string inStr)
        {
            var result = new IndexAndLength<int>() { Index = -1, Length = 0 };

            foreach (var item in _optionBinders)
            {
                Verify.IsNotNullOrEmpty(item);
                bool isVariadic = item.EndsWith(@"\*");

                result.Index = isVariadic
                    ? inStr.IndexOf(item.Substring(0, 1), StringComparison.InvariantCulture)
                    : inStr.IndexOf(item, StringComparison.InvariantCulture);

                if (0 > result.Index)
                    continue;

                var binder = isVariadic
                    ? GetVariableLengthBinderFromCandidate(inStr, result.Index, item)
                    : item;
                result.Length = binder.Length;
                break;
            }

            return result;
        }

        /// <summary>
        /// Get the specified verb.
        /// </summary>
        /// <param name="name">The verb to search for.</param>
        /// <param name="verb">The result upon success.</param>
        /// <returns>True if successful, false otherwise.</returns>
        /// <remarks>
        /// <para>This is protected so we can drive testing to take the failure path.</para>
        /// </remarks>
        protected bool GetLastVerbNameMatch(string name, out IVerb verb)
        {
            Verify.IsNotNullOrEmpty(name);
            verb = null;

            var result = _verbsFound.FindLast(x => x.Name == name);
            if (null != result)
            {
                verb = result;
                return true;
            }

            return false;
        }


        private string GetVariableLengthBinderFromCandidate(string inStr, int idx, string binderSpec)
        {
            // We expect a string of the form @"c\*", such that user intent
            // is to specify an indeterminate number of 'c' characters.
            Verify.IsTrue(1 == binderSpec.IndexOf(@"\*", StringComparison.InvariantCulture));

            int endIdx = idx + 1;
            while (inStr[endIdx] == binderSpec[0])
            {
                endIdx++;
            }

            return inStr.Substring(idx, endIdx - idx);
        }

        private int PrefixLength(string arg)
        {
            int length = 0;
            foreach (var prefix in _optionPrefixes)
            {
                if (arg.StartsWith(prefix))
                {
                    // Because we could have prefixes of the form '--', we want the length of longest match.
                    int temp = prefix.Length;

                    if (temp > length)
                    {
                        length = temp;
                    }
                }
            }

            return length;
        }

        private bool ProcessArg(string arg)
        {
            int prefixLength = PrefixLength(arg);

            if (prefixLength > 0)
            {
                string optionName = arg.Remove(0, prefixLength);
                return ProcessOptionCandidate(optionName);
            }
            return ProcessVerb(arg);
        }

        private bool ProcessArgs(IEnumerable<string> args)
        {
            bool result = true;

            if (null != args)
            {
                foreach (var item in args)
                {
                    if (ProcessArg(item))
                    {
                        continue;
                    }

                    result = false;
                    UsageErrorHandler.AddUnmatched(item);
                }
            }

            return result;
        }

        private bool ProcessOptionCandidate(string candidate)
        {
            string option;
            string value;

            if (!TrySplitOptionString(candidate, out option, out value)) return false;
            Verify.IsNotNullOrEmpty(option);
            Verify.IsNotNullOrEmpty(value);

            return TryUpdate(option, value);
        }

        private bool ProcessVerb(string candidate)
        {
            var optionsDb = OptionsDb.GetTheOptionsDb();
            foreach (var verb in optionsDb.Verbs)
            {
                if (verb.Name != candidate) continue;
                _currentVerb = candidate;
                Interlocked.Increment(ref _verbCount);
                if (_verbsFound.Exists(x => x.Name == verb.Name))
                {
                    var verbInstance = verb.Spawn();
                    if (null != verbInstance)
                    {
                        _verbsFound.Add(verbInstance);
                        return true;
                    }
                    else
                    {
                        UsageErrorHandler.AddError("Duplicate verb found:" + candidate + '.');
                        return false;
                    }
                }
                else
                {
                    _verbsFound.Add(verb);
                    return true;
                }
            }

            UsageErrorHandler.AddError("Failed to match verb: " + candidate + '.');
            return false;
        }

        private bool TrySplitOptionString(string inStr, out string option, out string value)
        {
            option = string.Empty;
            value = string.Empty;

            var binderSpecs = GetBinderStartAndLength(inStr);

            if (-1 == binderSpecs.Index)
            {
                // If there's no binder, and it looks like a switch,
                // then inStr might be a switch.  Look for the +/- at the end,
                // and convert value to true or false if it is found.
                if (inStr.EndsWith('-'))
                {
                    binderSpecs.Index = inStr.Length - 1;
                    value = "false";
                }
                else if (inStr.EndsWith('+'))
                {
                    binderSpecs.Index = inStr.Length - 1;
                    value = "true";
                }

                if (binderSpecs.Index < 1)
                {
                    return false;
                }
            }
            else
            {
                value = inStr.Substring(binderSpecs.Index + binderSpecs.Length);
            }

            option = inStr.Substring(0, binderSpecs.Index);

            return true;
        }

        private bool TryUpdate(string optionName, string value)
        {
            if (string.IsNullOrEmpty(_currentVerb))
            {
                return TryUpdateOption(optionName, value);
            }

            return TryUpdateVerbOption(optionName, value);
        }

        private bool TryUpdateVerbOption(string optionName, string value)
        {
            bool result = false;
            var optionsDb = OptionsDb.GetTheOptionsDb();
            IOption option;

            // We should not get this far into parsing if _currentVerb hasn't been matched.
            Verify.IsTrue(GetLastVerbNameMatch(_currentVerb, out var verb));
            option = verb.GetOption(optionName);

            if (null != option)
            {
                if (option.SetValue(value))
                {
                    result = true;
                }
                else
                {
                    UsageErrorHandler.AddError("Unable to update option '"
                                               + optionName
                                               + "' from value string `"
                                               + value
                                               + "`, for verb `"
                                               + _currentVerb
                                               + "`.");
                }
            }

            return result;
        }

        private bool TryUpdateOption(string optionName, string value)
        {
            var optionsDb = OptionsDb.GetTheOptionsDb();
            IOption option;

            if (!optionsDb.GetNonVerbOption(optionName, out option))
            {
                return false;
            }

            if (!option.SetValue(value))
            {
                UsageErrorHandler.AddError("Unable to update option '"
                                           +  optionName
                                           + "' from value string `"
                                           + value
                                           + "`.");
                return false;
            }

            return true;
        }
#endregion Private Methods

#region IDisposable Support
        // TODO: We may not need this to be IDisposable.
        // We started out with this opening files to secure them until
        // needed, but as of now, there are no files being opened be this
        // class.  When we add the '@responsfile` mechanism, we may need
        // to reconsider this.
        //
        // Leaving this in here for now, until we evaluate any performance
        // penalty/benefits of early disposal of the parser, to free up
        // application memory.

        private bool _disposedValue; // To detect redundant calls

        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _verbsFound = null;
                }

                // There are no unmanaged resources to free here at this time.
                // There used to be, but extensive refactoring has removed them.
                // 
                // There are no large fields to null out at this time.

                _disposedValue = true;
            }
        }

        // Override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~OptionsParser() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        /// <summary>
        /// This code added to correctly implement the disposable pattern.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // Uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
#endregion IDisposable Support
    }
}