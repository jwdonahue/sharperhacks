﻿using SharperHacks.Options;
using System;
using System.Threading;

// ReSharper disable UnusedParameter.Local

namespace Experiment
{
    internal class Program
    {
#if false
        private static readonly Verb _verb = new Verb(
            "verb1",
            new IOption[] {
                new VerbOption<int>(
                    false, 
                    "v1Opt1",
                    42,
                    new Usage("short", new[] {"longer"})
                    )
            },
            new Usage(
                "Verb1 short description.", 
                new[] {
                    "Long description line 1.",
                    "Long description line 2.",
                }
                )
        );

        private static readonly Usage _programUsage = new Usage("Experiment with Options classes.", null, null);

        // ReSharper disable once IdentifierTypo
        private static Option<int> Argx { get; } = new Option<int>(
            true,
            "_x",
            new[]
            {
                "T",
                "X",
            },
            new Usage("short", new[] {"longer"})
        );

        // Variable names are independent of option names.
        private static int X
        {
            get => Argx.Value;
            set => Argx.Value = value;
        }

        private static OptionsParser _optionsParser = new OptionsParser(new UsageErrorHandler());
#endif
        static void Main(string[] args)
        {
            Console.WriteLine("Friendly name:     " + AppDomain.CurrentDomain.FriendlyName);
            Console.WriteLine("Current directory: " + AppDomain.CurrentDomain.BaseDirectory);
            Console.WriteLine("Entry Assembly name: " + System.Reflection.Assembly.GetEntryAssembly()?.FullName);
            Console.WriteLine("Entry Assembly full name: " +
                              System.Reflection.Assembly.GetEntryAssembly()?.GetName().Name);
            Console.WriteLine("Environment.GetCommandLineArgs() result: \n");
            foreach (var item in Environment.GetCommandLineArgs())
            {
                Console.WriteLine(item);
            }

#if false
            using (var _optionsParser = new OptionsParser(new UsageErrorHandler()))
            {
            //_optionsParser = new OptionsParser();
                if (!_optionsParser.UpdateOptionsFromArguments(args))
                {
                    Console.WriteLine("Failed to process args:");
                }

                X = 42;

//                Console.WriteLine("_verb.Options[0] == " + _verb.Options.ToArray()[0].ValueString);

                Usage.Write(_programUsage);

                Console.ReadKey();
            }
#endif
        }
    }

}
