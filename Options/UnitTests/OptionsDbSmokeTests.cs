﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Options.Interfaces;

#pragma warning disable CS1591

namespace SharperHacks.Options.UnitTests
{
    /// <summary>
    /// Smoke tests the <see cref="OptionsDb"/> class.
    /// </summary>
    [TestClass]
    public class OptionsDbSmokeTests
    {
        [TestMethod]
        public void NonVerbOptions()
        {
            const string option1Name = "option1";
            string shortDescription = "short";
            string[] longDescription = new[] {"longer"};
            OptionsDb optionsDb = new OptionsDb();
            Assert.IsNotNull(optionsDb);
            Assert.AreEqual(0, optionsDb.Verbs.Count());
            Assert.AreEqual(0, optionsDb.NonVerbOptions.Count());

            var option1 = new Option<int>(
                true,
                option1Name,
                new Usage(
                    shortDescription, 
                    longDescription,
                    new UsageKeyScopePair(option1Name, UsageDocumentScope.OptionReadMe)
                    )
            );
            Assert.IsNotNull(option1);

            Assert.IsTrue(optionsDb.AddNonVerbOption(option1));
            Assert.AreEqual(0, optionsDb.Verbs.Count());
            Assert.AreEqual(1, optionsDb.NonVerbOptions.Count());

            Assert.IsFalse(optionsDb.GetNonVerbOption("dummy", out IOption optOut));
            Assert.IsNull(optOut);
            Assert.IsTrue(optionsDb.GetNonVerbOption(option1.Name, out optOut));
            Assert.IsNotNull(optOut);

            string option2Name = "Z";
            string[] aliases = {"y", "x"};
            var option2 = new Option<string>(
                false,
                option2Name,
                aliases,
                new Usage(
                    shortDescription, 
                    longDescription,
                    new UsageKeyScopePair(option2Name, UsageDocumentScope.OptionReadMe)
                    )
            );

            Assert.IsTrue(optionsDb.AddNonVerbOption(option2));
            Assert.IsTrue(optionsDb.GetNonVerbOption(option2Name, out optOut));
            foreach (var alias in option2.Aliases)
            {
                Assert.IsTrue(optionsDb.GetNonVerbOption(alias, out optOut));
                Assert.IsNotNull(optOut);
                Assert.AreEqual(option2.Aliases.ToArray()[0], optOut.Aliases.ToArray()[0]);
            }

#if false
// There can be only one.
        var option3 = new Option<string>(
            false,
            option2Name,
            new Usage(shortDescription, longDescription)
        );
        // Use of existing name is rejected.
        // TODO           Assert.IsFalse(optionsDb.AddNonVerbOption(option3));

        // Use of existing aliases is rejected.
        var option4 = new Option<string>(
            false,
            "Y",
            aliases,
            new Usage(shortDescription, longDescription)
        );
#endif
            // TODO           Assert.IsFalse(optionsDb.AddNonVerbOption(option4));
        }

        private class TestVerb1 : VerbBase
        {
            public VerbOption<int> option1 = new VerbOption<int>(true, "option1");
            public VerbOption<bool> option2 = new VerbOption<bool>(true, "option1");

            public TestVerb1(string name) : base(name, new Usage()) { }

            public override int Run() => 0;
        }

        [TestMethod]
        public void Verbs()
        {
            OptionsDb optionsDb = new OptionsDb();
            Assert.IsNotNull(optionsDb);
            Assert.AreEqual(0, optionsDb.Verbs.Count());
            Assert.AreEqual(0, optionsDb.NonVerbOptions.Count());

            var verb1 = new TestVerb1("verb1");
            Assert.IsNotNull(verb1);

            Assert.IsTrue(optionsDb.AddVerb(verb1));
            Assert.AreEqual(1, optionsDb.Verbs.Count());
            Assert.AreEqual(0, optionsDb.NonVerbOptions.Count());
        }
    }
}