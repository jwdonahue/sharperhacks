﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.IO;
using System;

namespace SharperHacks.Options.Examples.UnitTests
{
    [TestClass]
    public class Example2SmokeTest
    {
        [TestMethod]
        public void SmokeIt()
        {
            //
            // These capture output blocks must always be run sequentially.
            //

            // Valid args test.
            using (var output = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
            {
                string[] args = new string[] {"SayHello", "-To:Dave"};
                Example2.Main(args);
                string spew = output.CapturedOutput;
                Assert.IsFalse(spew.Contains("Error"));
                Assert.IsTrue(spew.Contains("Hello Dave"));
            }

            // Invalid args test.
            using (var output = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
            {
                string[] args = new string[] {"-badArg"};
                Example2.Main(args);
                string spew = output.CapturedOutput;
                Assert.IsTrue(spew.Contains(args[0]));
                Assert.IsTrue(spew.Contains("Failed to process args:"));


                Assert.IsTrue(spew.Contains("SayHello"));
                Assert.IsTrue(spew.Contains("Say hello, optionally to the name provided by the 'To' parameter."));
                Assert.IsTrue(spew.Contains("SayHello -To:Dave"));
                Assert.IsTrue(spew.Contains("Try 'Example2 SayHello -To:somebody'"));
            }
        }
    }
}