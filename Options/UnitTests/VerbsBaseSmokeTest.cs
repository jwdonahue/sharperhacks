﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.UnitTests
{
    /// <summary>
    /// Test adapter. Provides Pre/PostVerbsRun() overrides .
    /// </summary>
    class TestVerbsBase : VerbsBase
    {
        public int PreVerbsReturns { get; set; }
        public int PostVerbsReturns { get; set; }

        public TestVerbsBase(
            string[] args,
            bool requiresVerb,
            bool allowMultipleVerbs,
            bool continueOnVerbFailure,
            int preVerbsReturns,
            int postVerbsReturns)
            : base(
                  args,
                  new UsageErrorHandler(),
                  requiresVerb,
                  allowMultipleVerbs,
                  continueOnVerbFailure)
        { 
            PreVerbsReturns = postVerbsReturns;
            PostVerbsReturns = postVerbsReturns;
        }

        protected override int PreVerbsRun() => PreVerbsReturns;
        protected override int PostVerbsRun() => PostVerbsReturns;
    }

    class TestVerbsWithoutProgramOptions : TestVerbsBase
    {
        public TestVerbsWithoutProgramOptions(
            string[] args,
            bool requiresVerb,
            bool allowMultipleVerbs,
            bool continueOnVerbFailure)
            : base(
                  args, 
                  requiresVerb, 
                  allowMultipleVerbs, 
                  continueOnVerbFailure,
                  0, // PreVerbsRun() returns zero.
                  0) // PostVerbsRun() returns zero.
        { }
    }

    [TestClass]
    public class VerbsBaseSmokeTest
    {
        // VerbsBase and derived classes depend on underlying static resources.
        // There normally wouldn't be more than one of them in a program, but we
        // try to run as many parallel unit tests as possible, so these have to
        // be protected from concurrency, or the tests will fail.

        [TestMethod]
        public void VerbsBaseLongConstructorNoOverrides()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);
                string[] args = new string[] { verb1Name, verb2Name };
                var verbs = new VerbsBase(
                    args,
                    new UsageErrorHandler(),
                    true,
                    true,
                    false,
                    new string[] {"-"}, 
                    new string[] {":"},
                    null
                );

                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
                Assert.IsTrue(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(0, result);
            
                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void JustTheVerbsRequired()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);
                string[] args = new string[] { verb1Name, verb2Name };
                var verbs = new TestVerbsWithoutProgramOptions(args, true, true, false);

                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
                Assert.IsTrue(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(0, result);
            
                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void JustTheVerbsNotRequired()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);
                var verbs = new TestVerbsWithoutProgramOptions(new string[] { }, false, true, false);

                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
                Assert.IsFalse(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(0, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void FailVerbRun()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), -1);
                string[] args = new string[] { verb1Name, verb2Name };
                var verbs = new TestVerbsWithoutProgramOptions(args, false, true, false);

                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
                Assert.IsTrue(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(-1, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void VerbRequiredButNotFound()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);
                var verbs = new TestVerbsWithoutProgramOptions(null, true, true, false);

// TODO: Why does this fail?                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(-1, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void FailPreVerbsRun()
        {
            var verb1Name = "Verb1";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verbs = new TestVerbsWithoutProgramOptions(
                    new string[] { "Verb1" }, 
                    false, 
                    false, 
                    false) { PreVerbsReturns = -1 };

                Assert.IsFalse(verbs.HasParseErrors);
            
                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(verbs.PreVerbsReturns, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void FailPostVerbsRun()
        {
            var verb1Name = "Verb1";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verbs = new TestVerbsWithoutProgramOptions(
                    new string[] { "Verb1" }, 
                    false, 
                    false, 
                    false) { PostVerbsReturns = -1 };

                Assert.IsFalse(verbs.HasParseErrors);
            
                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(verbs.PostVerbsReturns, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void AllowMultipleVerbsIsFalseAndVerbsParsedGreaterThanOne()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = new VerbWithOptions(verb1Name, new Usage(), 0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);
                string[] args = new string[] { verb1Name, verb2Name };

                var verbs = new TestVerbsWithoutProgramOptions(args, false, false, false);

                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
//                Assert.IsFalse(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(-1, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }

        [TestMethod]
        public void WhenNoVerbsFoundAndVerbsRequired()
        {
            var verb1Name = "Verb1";
            var verb2Name = "Verb2";

            lock (this)
            {
                var verb1 = 
                    new VerbWithOptions(
                        verb1Name, 
                        new Usage("Simple", new UsageKeyScopePair(verb1Name, UsageDocumentScope.VerbOptionReadMe)), 
                    0);
                var verb2 = new VerbWithOptions(verb2Name, new Usage(), 0);

                var verbs = new TestVerbsWithoutProgramOptions(null, true, false, false);

// TODO: Should there be parse errors here?                Assert.IsFalse(verbs.HasParseErrors);
                Assert.IsFalse(verbs.HasUnmatched);
                Assert.IsFalse(verbs.HasVerbs);

                int result = verbs.RunIfParsedSuccessfully();

                Assert.AreEqual(-1, result);

                verbs.Parser.Reset();
                OptionsDb.Reset();
            }
        }
    }
}
