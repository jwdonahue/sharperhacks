﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using SharperHacks.Options.Interfaces;

#pragma warning disable CS1591

namespace SharperHacks.Options.UnitTests
{
    [TestClass]
    public class OptionsParserSmokeTests
    {
        private readonly string[] _optionArgs = {"-X", "/y", "--z"};
        private readonly string[] _verbArgs = {"verb1", "verb2", "verb3"};

        private static void RundownValues(int unmatchedExpected, OptionsParser optionsParser)
        {
            Assert.IsFalse(optionsParser.RequiresVerb);
            Assert.IsNotNull(optionsParser.UsageErrorHandler.Unmatched);
            Assert.AreEqual(unmatchedExpected, optionsParser.UsageErrorHandler.Unmatched.Count());
        }

        [TestMethod]
        public void CheckPreinitializeDefaults()
        {
            OptionsDb.Reset();
            OptionsParser optionsParser = new OptionsParser(new UsageErrorHandler());
            RundownValues(0, optionsParser);
        }

        [TestMethod]
        public void UpdateFromArguments_UnmatchableOptions()
        {
            OptionsDb.Reset();
            OptionsParser optionsParser = new OptionsParser(new UsageErrorHandler());
            Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(_optionArgs));
            RundownValues(3, optionsParser);
            optionsParser.Reset();
            RundownValues(0, optionsParser);
        }

        [TestMethod]
        public void UpdateFromArguments_UnmatchableVerbs()
        {
            OptionsDb.Reset();
            OptionsParser optionsParser = new OptionsParser(new UsageErrorHandler());
            Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(_verbArgs));
            RundownValues(3, optionsParser);
            optionsParser.Reset();
            RundownValues(0, optionsParser);
        }

        [TestMethod]
        public void UpdateOptionsFromArguments_WhenVerbRequiredUnmatchable()
        {
            OptionsDb.Reset();
            var optionsParser = new OptionsParser(new UsageErrorHandler())
            {
                RequiresVerb = true
            };
            Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(_optionArgs));
            Assert.AreEqual(3, optionsParser.UsageErrorHandler.Unmatched.Count());
            Assert.AreEqual(1, optionsParser.UsageErrorHandler.ParseErrors.Count());
            // TODO: Validate the error messages!

            Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(_verbArgs));
            Assert.AreEqual(6, optionsParser.UsageErrorHandler.Unmatched.Count());
            Assert.AreEqual(5, optionsParser.UsageErrorHandler.ParseErrors.Count());
            // TODO: Validate the error messages!

            optionsParser.Reset();
            RundownValues(0, optionsParser);
        }

        private class TestVerb1 : VerbBase
        {
            public VerbOption<int> option1 = new VerbOption<int>(true, "option1");
            public VerbOption<bool> option2 = new VerbOption<bool>(true, "option2");

            public TestVerb1(string name) : base(name, new Usage())
            {
                // Avoid unused variable warnings from VS/Resharper
                Assert.IsNotNull(option1);
                Assert.IsNotNull(option2);
            }

            public override int Run() => 0;
        }


        [TestMethod]
        public void UpdateOptionsFromArguments_WithValidOptionsAndVerbs()
        {
            OptionsDb.Reset();
            using (OptionsParser optionsParser = new OptionsParser(new UsageErrorHandler()))
            {
                Option<int> option1 = new Option<int>(
                    true,
                    "Opt1",
                    new Usage("short", new[] { "longer" }, new UsageKeyScopePair("Opt1", UsageDocumentScope.OptionReadMe))
                    );
                Assert.IsNotNull(option1);

                var verb1 = new TestVerb1("verb1");
                var verb2 = new TestVerb1("verb2");

                // Avoid unused local variable warnings from VS/Resharper
                Assert.IsNotNull(verb1);
                Assert.IsNotNull(verb2);

                Assert.IsNotNull(optionsParser.VerbsFound);
                Assert.IsFalse(optionsParser.HasVerbs);

                string[] args = { "-Opt1:-1" };
                Assert.IsTrue(optionsParser.UpdateOptionsFromArguments(args));
                Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(new[] { "-option1:not a number" }));
                Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(new[] { "-bogusOption:whatever" }));

                Assert.IsTrue(optionsParser.UpdateOptionsFromArguments(new [] {"verb1", "-option1:42" }));
                Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(new[] { "-Opt1:not a number" }));
                Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(new[] { "-TotallyBogus:man" }));

                Assert.IsTrue(optionsParser.UpdateOptionsFromArguments(new[] {"verb2", "-option1:42"}));

                Assert.IsFalse(optionsParser.UpdateOptionsFromArguments(new [] {"verb2", "-option1:blah"}));

                Assert.IsTrue(optionsParser.HasVerbs);
                Assert.AreEqual(2, optionsParser.VerbsFound.Count());

                optionsParser.Reset();
                OptionsDb.Reset(); // Because we have actual verbs and options.
                RundownValues(0, optionsParser);
            }
        }

        [TestMethod]
        public void UpdateOptionsFromArguments_OptionPreceedsVerb()
        {
            OptionsDb.Reset();
            using (OptionsParser optionsParser = new OptionsParser(new UsageErrorHandler()))
            {
                var option1 = new Option<int>(
                    true,
                    "Opt1",
                    new Usage("short", new[] {"longer"}, new UsageKeyScopePair("Opt1", UsageDocumentScope.OptionReadMe))
                );
                Assert.IsNotNull(option1);

                var verb1 = new TestVerb1("verb1");
                var verb2 = new TestVerb1("verb2");

                // Avoid unused local variable warnings from VS/Resharper
                Assert.IsNotNull(verb1);
                Assert.IsNotNull(verb2);

                string[] args =
                {
                    "-Opt1:-1",
                    "verb1",
                    "-option1:42",
                    "verb2",
                    "-option1:7",
                };

                Assert.IsTrue(optionsParser.UpdateOptionsFromArguments(args));

                optionsParser.Reset();
                OptionsDb.Reset(); // Because we have actual verbs and options.
            }
        }

        [TestMethod]
        public void BadOptionData()
        {
            using (var parser = new OptionsParser(new UsageErrorHandler()))
            {
                var option1 = new Option<int>(
                    true,
                    "string",
                    new Usage("short", new[] { "longer" }, new UsageKeyScopePair("string", UsageDocumentScope.OptionReadMe))
                );

                var args = new[] {"--string ShouldBeInt"};

                Assert.IsFalse(parser.UpdateOptionsFromArguments(args));
                Assert.AreEqual(1, parser.UsageErrorHandler.Unmatched.Count());
                Assert.AreEqual(1, parser.UsageErrorHandler.ParseErrors.Count());
            }
        }

        [TestMethod]
        public void Constructor_ErrorHandler_Prefix_And_Binder()
        {
            var errorHandler = new UsageErrorHandler();
            var prefixes = new[] {"Option:"};
            var binders = new[] {" "};

            var parser = new OptionsParser(errorHandler, prefixes, binders);
            Assert.AreEqual(prefixes[0], parser.OptionPrefixes.ToArray()[0]);
            Assert.AreEqual(binders[0], parser.OptionBinders.ToArray()[0]);
        }

        [TestMethod]
        public void VariadicBinders()
        {
            string optionName = "VariadicTestOption";
            string[] binders = new[] {@" \*"};
            string[] prefixes = new[] {"--", "-", "/"};
            // ReSharper disable once UnusedVariable
            var option = new Option<int>(false, optionName);
            var parser = new OptionsParser(new UsageErrorHandler(), prefixes, binders);
            parser.UpdateOptionsFromArguments(new[] {"-" + optionName + "   2"});
            Assert.AreEqual(0, parser.UsageErrorHandler.Unmatched.Count());
            Assert.AreEqual(0, parser.UsageErrorHandler.ParseErrors.Count());
        }

        [TestMethod]
        public void SwitchCandidate()
        {
            string optionName = "switch";
            string[] binders = new[] {":"};
            string[] prefixes = new[] {"--", "-", "/"};
            // ReSharper disable once UnusedVariable
            var option = new Option<bool>(false, optionName);
            var parser = new OptionsParser(new UsageErrorHandler(), prefixes, binders);
            parser.UpdateOptionsFromArguments(new[] {"-" + optionName + "+"});
            Assert.AreEqual(0, parser.UsageErrorHandler.Unmatched.Count());
            Assert.AreEqual(0, parser.UsageErrorHandler.ParseErrors.Count());
            parser.UpdateOptionsFromArguments(new[] {"-" + optionName + "-"});
            Assert.AreEqual(0, parser.UsageErrorHandler.Unmatched.Count());
            Assert.AreEqual(0, parser.UsageErrorHandler.ParseErrors.Count());
        }

        private class MultiVerb : VerbBase
        {
            public VerbOption<int> Opt1 = new VerbOption<int>(false, "Opt1");

            public override IVerb Spawn() => new MultiVerb(Name, Usage);

            public MultiVerb(string name, IUsage usage) : base(name, usage) => IsSingleton = false;

            public override int Run() => 0;
        }

        [TestMethod]
        public void AllowMultipleVerbInstances()
        {
            var usage = new Usage();
            var myVerb = new MultiVerb("PartyVerb", usage);
            var parser = new OptionsParser(new UsageErrorHandler());
            var args = new string[] {"PartyVerb", "-Opt1:1", "PartyVerb", "-Opt1:2", "PartyVerb", "-Opt1:3"};

            Assert.IsTrue(parser.UpdateOptionsFromArguments(args));

            Assert.AreEqual(3, parser.VerbCount);

            int optionValue = 1;
            foreach (var item in parser.VerbsFound)
            {
                Assert.AreEqual(optionValue, int.Parse(item.GetOption("Opt1").ValueString));
                optionValue++;
            }
        }

        // In order to get full coverage of OptionsParser.GetLastVerbNameMatch(string, out IVerb)
        // it was necessary to change it from private to protected, and wrap the class so we can
        // verify the no match found path behaves correctly.
        class OptionsParserTest : OptionsParser
        {
            public OptionsParserTest() : base(new UsageErrorHandler()) { }

            public bool TestGetLastVerbNameMatch(out IVerb verb)
            {
                return GetLastVerbNameMatch("NonExistantVerb", out verb);
            }
        }

        [TestMethod]
        public void CoverOptionsParser_GetLastVerbNameMatch()
        {
            var parser = new OptionsParserTest();

            Assert.IsFalse(parser.TestGetLastVerbNameMatch(out var verb));
            Assert.IsNull(verb);
        }
    }
}