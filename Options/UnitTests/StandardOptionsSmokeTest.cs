﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Diagnostics;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.UnitTests
{
    [TestClass]
    public class StandardOptionsSmokeTest
    {
        // StandardOptions defaults to being a singleton, so we need to prevent
        // test methods from violating that constraint while achieving full coverage.
        // Hence the lock. Out test methods will run sequentially and reset the unit under test.
        private static readonly object _lock = new Object();

        [TestMethod]
        public void StandardOptions_DefaultConstructor()
        {
            lock (_lock)
            {
                IStandardOptions stdOpt1 = new StandardOptions.StandardOptions();

                // Verify the default state of stdOpt1.
                Assert.IsTrue(stdOpt1.HasStandardHelpOption);
                Assert.IsNotNull(stdOpt1.StandardHelp);

                // TODO: Change this when we have a clue how to implement diagnostics.
                Assert.IsFalse(stdOpt1.HasStandardDiagnosticsOption);
                Assert.IsNull(stdOpt1.StandardDiagnostic);

                StandardOptions.StandardOptions.Reset();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(VerifyException))]
        public void StandardOptions_DefaultConstructor_ThrowsExceptionIfNotSingleton()
        {
            lock (_lock)
            {
                var stdOpt1 = new StandardOptions.StandardOptions();
                var stdOpt2 = new StandardOptions.StandardOptions();

                StandardOptions.StandardOptions.Reset();
            }
        }

        [TestMethod]
        public void StandardOptions_ThereCanBeMoreThanOne()
        {
            lock (_lock)
            {
                var stdOpt1 = new StandardOptions.StandardOptions(false);
                var stdOpt2 = new StandardOptions.StandardOptions(false);

                StandardOptions.StandardOptions.Reset();
            }
        }
    }
}