// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Options.Interfaces;
using System.Linq;

#pragma warning disable CS1591

namespace SharperHacks.Options.UnitTests
{
    [TestClass]
    public class OptionSmokeTests
    {
        private readonly string _shortDescription = "short";
        private readonly string[] _longDescription = {"longer"};

        [TestMethod]
        public void Option_bool_string()
        {
            const string optionName = "intOption";
            var intOption = new Option<int>(true, optionName);
            Assert.IsNotNull(intOption);
//            Assert.AreEqual(typeof(int), intOption.GetOptionType());

            // Verify IOption.Aliases is empty and not null.
            Assert.IsNotNull(intOption.Aliases);
            Assert.AreEqual(Enumerable.Empty<string>(), intOption.Aliases);

            // Verify IOption.IsOptional matches the isOptional parameter passed to the constructor.
            Assert.IsTrue(intOption.IsOptional);

            // Verify IOption.Name matches the name parameter passed to the constructor.
            Assert.AreEqual(optionName, intOption.Name);
            Assert.AreEqual(default(int), intOption.DefaultValue);
            Assert.AreEqual(default(int), intOption.Value);
            Assert.IsTrue(intOption.SetValue("5"));
            Assert.AreEqual("5", intOption.ValueString);
            Assert.AreEqual(5, intOption.Value);
            Assert.AreEqual(default(int), intOption.DefaultValue);
            Assert.IsFalse(intOption.SetValue("not a number"));

        }

        /// <summary>
        /// Test Option(bool,string,IUsage) constructor and all IOption properties/methods.
        /// </summary>
        [TestMethod]
        public void Option_bool_string_IUsage_constructor()
        {
            const string optionName = "intOption";
            var intOption = new Option<int>(
                true, 
                optionName, 
                new Usage(
                    _shortDescription, 
                    _longDescription,
                    new UsageKeyScopePair(optionName, UsageDocumentScope.OptionReadMe)
                    )
                );
            Assert.IsNotNull(intOption);
//            Assert.AreEqual(typeof(int), intOption.GetOptionType());

            // Verify IOption.Aliases is empty and not null.
            Assert.IsNotNull(intOption.Aliases);
            Assert.AreEqual(Enumerable.Empty<string>(), intOption.Aliases);

            // Verify IOption.IsOptional matches the isOptional parameter passed to the constructor.
            Assert.IsTrue(intOption.IsOptional);

            // Verify IOption.Name matches the name parameter passed to the constructor.
            Assert.AreEqual(optionName, intOption.Name);
            Assert.AreEqual(_shortDescription, intOption.Usage.ShortDescription);
            Assert.AreEqual(_longDescription.First(), intOption.Usage.LongDescription.First());
            Assert.AreEqual(default(int), intOption.DefaultValue);
            Assert.AreEqual(default(int), intOption.Value);
            Assert.IsTrue(intOption.SetValue("5"));
            Assert.AreEqual("5", intOption.ValueString);
            Assert.AreEqual(5, intOption.Value);
            Assert.AreEqual(default(int), intOption.DefaultValue);
            Assert.IsFalse(intOption.SetValue("not a number"));
        }

        [TestMethod]
        public void Option_string_string_string_T_constructor()
        {
            const string optionName = "boolOption";
            IOption<bool> boolOption = new Option<bool>(
                false,
                optionName,
                true,
                new Usage(
                    _shortDescription, 
                    _longDescription,
                    new UsageKeyScopePair(optionName, UsageDocumentScope.OptionReadMe))
            );
            Assert.IsNotNull(boolOption);
            Assert.IsFalse(boolOption.IsOptional);
            Assert.AreEqual(0, boolOption.Aliases.Count());
            Assert.AreEqual(optionName, boolOption.Name);
            Assert.AreEqual(_shortDescription, boolOption.Usage.ShortDescription);
            Assert.AreEqual(_longDescription.First(), boolOption.Usage.LongDescription.First());
            Assert.AreEqual(true, boolOption.DefaultValue);
            Assert.AreEqual(true, boolOption.Value);
        }

        [TestMethod]
        public void Option_stringArray_string_string_constructor()
        {
            string[] aliases = {"so", "s"};
            var option = new Option<string>(
                true,
                "stringOption",
                aliases,
                new Usage(
                    _shortDescription, 
                    _longDescription,
                    new UsageKeyScopePair("stringOption", UsageDocumentScope.OptionReadMe)
                    )
            );
            Assert.IsNotNull(option);
            Assert.AreEqual(2, option.Aliases.Count());
            string[] aliases2 = option.Aliases.ToArray();
            Assert.AreEqual(aliases[0], aliases2[0]);
            Assert.AreEqual(aliases[1], aliases2[1]);
            Assert.AreEqual(_shortDescription, option.Usage.ShortDescription);
            Assert.AreEqual(_longDescription.First(), option.Usage.LongDescription.First());
            Assert.AreEqual(default(string), option.DefaultValue);
            Assert.AreEqual(default(string), option.Value);
        }

        [TestMethod]
        public void Option_stringArray_string_string_T_constructor()
        {
            string testString = "test";
            string[] aliases = {"s", "so"};
            var option = new Option<string>(
                true,
                "stringOption",
                aliases,
                testString,
                new Usage(
                    _shortDescription, 
                    _longDescription,
                    new UsageKeyScopePair("stringOption", UsageDocumentScope.OptionReadMe))
            );
            Assert.IsNotNull(option);
            Assert.AreEqual(2, option.Aliases.Count());
            string[] aliases2 = option.Aliases.ToArray();
            Assert.AreEqual(aliases[0], aliases2[0]);
            Assert.AreEqual(aliases[1], aliases2[1]);
            Assert.AreEqual(_shortDescription, option.Usage.ShortDescription);
            Assert.AreEqual(_longDescription.First(), option.Usage.LongDescription.First());
            Assert.AreEqual(testString, option.DefaultValue);
            Assert.AreEqual(testString, option.Value);
        }

        [TestMethod]
        public void CreateOptionFactory_string_stringArray_bool_T_FileInfo()
        {
            string name = "name";

            Option<bool> boolOption1 = Option<bool>.CreateOption(name, null);
            Option<bool> boolOption2 = Option<bool>.CreateOption(name, null);

            Assert.IsFalse(boolOption1.Value);
            Assert.AreEqual(boolOption1.Usage.KeyScopePair.GetHashCode(), boolOption2.Usage.KeyScopePair.GetHashCode());

            Option<int> intOption = Option<int>.CreateOption(
                "opt",
                null,
                false,
                default(int),
                new FileInfo("opt.readme.txt")
            );
            Assert.AreEqual(default(int), intOption.Value);
        }

        [TestMethod]
        public void CreateOptionFactory_string_stringArray_bool_string_stringArray_stringArray_T()
        {
            string name = "name";

            Option<bool> boolOption1 = Option<bool>.CreateOption(name, null, true, "short", null, null);
            Option<bool> boolOption2 = Option<bool>.CreateOption(name, null, true, "short", null, null);

            Assert.IsFalse(boolOption1.Value);
            Assert.AreEqual(boolOption1.Usage.KeyScopePair.GetHashCode(), boolOption2.Usage.KeyScopePair.GetHashCode());
        }
    }
}
