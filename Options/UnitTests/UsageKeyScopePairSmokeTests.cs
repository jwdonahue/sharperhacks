﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SharperHacks.Options.Interfaces.UnitTests
{
    [TestClass]
    public class UsageKeyScopePairSmokeTests
    {
        [TestMethod]
        public void SmokeIt()
        {
            string[] keys = {"key1", "key2", "key3"};

            int keyIdx = 0;
            foreach (UsageDocumentScope scope in Enum.GetValues(typeof(UsageDocumentScope)))
            {
                var ksp1 = new UsageKeyScopePair(keys[keyIdx], scope);
                // ksp2 == ksp1
                var ksp2 = new UsageKeyScopePair(keys[keyIdx], scope);
                // ksp3 != ksp1 on the key name.
                var ksp3 = new UsageKeyScopePair(keys[(keyIdx +1) %3], scope);
                // ksp4 != ksp1 on Scope.
                var ksp4 = new UsageKeyScopePair(keys[keyIdx], scope + 1);

                Assert.AreEqual(scope, ksp1.Scope);
                Assert.AreEqual(keys[keyIdx], ksp1.Key);

                Assert.AreEqual(ksp1.Key, ksp2.Key);
                Assert.AreEqual(ksp1.Scope, ksp2.Scope);
                Assert.IsTrue(ksp1.Equals(ksp2));

                if (ksp1 != ksp2)
                {
                    Assert.Fail();
                }

                Assert.AreEqual(ksp1, ksp2);
                Assert.AreNotEqual(ksp1, ksp3);
                Assert.AreNotEqual(ksp1, ksp4);

                if (keyIdx == 3) keyIdx = 0;
            }

            var ksp = new UsageKeyScopePair("NotNull", UsageDocumentScope.OptionReadMe);

            if (null == ksp) Assert.Fail();
            if (ksp == null) Assert.Fail();
            Assert.IsTrue(null != ksp);
            Assert.IsTrue(ksp != null);
            Assert.IsFalse(ksp.Equals((object)null));
            Assert.IsTrue( ((UsageKeyScopePair) null) == ((UsageKeyScopePair) null));
        }
    }
}