﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
// ReSharper disable IdentifierTypo

#pragma warning disable CS1591

namespace SharperHacks.Options.UnitTests
{
    public class TestOptions : OptionsBase
    {
        // ReSharper disable once UnusedMember.Global
        public Option<int> Opt1 = new Option<int>(true, "Opt1");

        public TestOptions(string[] args)
            : base(args, new UsageErrorHandler())
        {
        }

        public TestOptions(string[] args, string[] prefixes, string[] binders)
            : base(args, new UsageErrorHandler(), prefixes, binders)
        {
        }

        public TestOptions(string[] args, string[] prefixes, string[] binders, FileInfo usageFile)
            : base(args, new UsageErrorHandler(), prefixes, binders, usageFile)
        {
        }

        protected override int Run()
        {
            return 0;
        }
    }

    [TestClass]
    public class OptionsBaseSmokeTest
    {
        private static object classLock = new object();

        [TestMethod]
        public void OptionsBase_args_errorhandler()
        {
            lock (classLock)
            {
                TestOptions testOptions = new TestOptions(new[] {"--Opt1:42"});
                Assert.IsFalse(testOptions.HasParseErrors);
                Assert.IsFalse(testOptions.HasUnmatched);
            }
        }

        [TestMethod]
        public void OptionsBase_args_errorhandler_prefixes_binders()
        {
            lock (classLock)
            {
                string[] args = new[] {"&Opt1<>42"};
                string[] prefixes = new[] {"&"};
                string[] binders = new[] {"<>"};

                TestOptions testOptions = new TestOptions(args, prefixes, binders);
                Assert.IsFalse(testOptions.HasParseErrors);
                Assert.IsFalse(testOptions.HasUnmatched);
            }
        }

        [TestMethod]
        public void OptionsBase_args_errorhandler_prefixes_binders_usage()
        {
            lock (classLock)
            {
                string[] args = new[] {"&Opt1<>42"};
                string[] prefixes = new[] {"&"};
                string[] binders = new[] {"<>"};

                TestOptions testOptions = new TestOptions(args, prefixes, binders, Usage.GetReadMeFileInfo());
                Assert.IsFalse(testOptions.HasParseErrors);
                Assert.IsFalse(testOptions.HasUnmatched);
            }
        }

    }
}
