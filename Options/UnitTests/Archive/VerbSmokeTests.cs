﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Options;
using System.Linq;

#pragma warning disable CS1591

namespace UnitTests
{
#if false
    [TestClass]
    public class VerbSmokeTests
    {

        [TestMethod]
        public void ConstructorAndIVerb()
        {
            string verb1Name = "verb1";
            string[] verb1Aliases = {"v1"};
            string verb1ShortDescription = "Short description.";
            string[] verb1LongDescription =
            {
                "Long description, line 1.",
                "Long description, line 2.",
            };
            string[] optionLongDescription = new string[] { "longer" };
            var verb1Option1 = new VerbOption<int>(
                false, 
                "v1Opt1", 
                verb1Aliases,
                -1,
                new Usage("Short", optionLongDescription)
                );
            var verb1Option2 = new VerbOption<bool>(
                false, 
                "v1Opt2", 
                new Usage("Short", optionLongDescription)
                );
            var verb1Options = new IOption[] {verb1Option1, verb1Option2};

            Verb verb1 = new Verb(
                verb1Name,
                verb1Options,
                new Usage(verb1ShortDescription, verb1LongDescription)
                );
            Assert.IsNotNull(verb1);
            Assert.AreEqual(verb1Name, verb1.Name);
            Assert.AreEqual(verb1ShortDescription, verb1.Usage.ShortDescription);
            Assert.AreEqual(verb1Options.Length, verb1.TestOptions.Count());
            Assert.AreEqual(verb1LongDescription.Length, verb1.Usage.LongDescription.Count());
            for (int idx = 0; idx < verb1Options.Length; idx++)
            {
                Assert.AreEqual(verb1Options[idx], verb1.TestOptions.ToArray()[idx]);
            }
            for(int idx = 0; idx < verb1LongDescription.Length; idx++)
            {
                Assert.AreEqual(verb1LongDescription[idx], verb1.Usage.LongDescription.ToArray()[idx]);
            }

            var option = verb1.GetOption(verb1Option1.Name);
            Assert.AreEqual(verb1Option1.Name, option.Name);

            string[] verb1OptionAliases = verb1Option1.Aliases.ToArray();
            option = verb1.GetOption(verb1OptionAliases[0]);
            string[] optionAliases = option.Aliases.ToArray();
            Assert.AreEqual(verb1OptionAliases[0], optionAliases[0]);

            option = verb1.GetOption("bogusOptionName");
            Assert.IsNull(option);

            // Verb options should not leak into the non-verb option domain.
            IOption result;
            Assert.IsFalse(OptionsDb.GetTheOptionsDb().GetNonVerbOption("v1Opt1", out result));
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void RunThrowsNotImplementedException()
        {
            new Verb("v1", new [] { new VerbOption<bool>(true, "boggie") }, new Usage("blah")).Run();
        }
    }
#endif
}