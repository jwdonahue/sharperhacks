﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Options.Interfaces;
using System;
using System.Linq;

#pragma warning disable CS1591

namespace SharperHacks.Options.UnitTests
{
    [TestClass]
    public class VerbBaseSmokeTests
    {
        private class VerbWithOptions : VerbBase
        {
            public VerbOption<int> IntOption = new VerbOption<int>(true, "IntOption");
            public VerbOption<bool> BoolOption = new VerbOption<bool>(true, "BoolOption", new Usage());
            public VerbOption<string> StringOption = new VerbOption<string>(true, "StringOption", "New Default", new Usage());
            public VerbOption<double> DoublOptionWithAliases = new VerbOption<double>(
                true, 
                "DoubleOptionWithAliases", 
                new [] {"DOWA"}, 
                new Usage());
            public VerbOption<char> CharOptionWithAliasesAndDefault = new VerbOption<char>(
                true,
                "CharOptionWithAliasesAndDefault",
                new [] {"COWAAD"},
                'c',
                new Usage());

            public VerbWithOptions(string name, IUsage usage) : base(name, usage) { }

            public override int Run() => 0;
        }

        private class VerbNoOptions : VerbBase
        {
            public VerbNoOptions(string name, IUsage usage) : base(name, usage) { }
            public override int Run() => 0;
        }

        [TestMethod]
        public void VerbBase_name_usage()
        {
            var usage = new Usage();
            var myVerb = new VerbWithOptions("v1", usage);
            Assert.IsTrue(myVerb.Options.Contains(myVerb.IntOption));

            var option = myVerb.GetOption("IntOption");
            Assert.IsNotNull(option);
            Assert.AreEqual("IntOption", option.Name);
            Assert.AreEqual(default(int), int.Parse(option.ValueString));
            myVerb.IntOption.SetValue("42");
            option = myVerb.GetOption("IntOption");
            Assert.AreEqual(42, int.Parse(option.ValueString));
            Assert.AreEqual(0, myVerb.Run());

            Assert.IsNotNull(myVerb.Usage);

            var verbNoOptions = new VerbNoOptions("v2", usage);
            Assert.IsFalse(verbNoOptions.Options.Any());
            Assert.IsNull(verbNoOptions.GetOption("IntOption"));
            Assert.AreEqual(0, verbNoOptions.Run());
        }
    }
}