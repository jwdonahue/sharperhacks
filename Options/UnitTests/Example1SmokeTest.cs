﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using SharperHacks.IO;

namespace SharperHacks.Options.Examples.UnitTests
{
    [TestClass]
    public class Example1SmokeTest
    {
        [TestMethod]
        public void ValidArgSmokeTest()
        {
            using (var output = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
            {
                string[] args = new string[] { "-SayHello:true" };
                Example1.Main(args);
                string spew = output.CapturedOutput;
                Assert.IsTrue(spew.Contains("Hello World!"));
            }
        }

        [TestMethod]
        public void InvalidArgSmokeTest()
        {
            // Unit test FriendlyName != Example1, so we make a copy of
            // the readme file that the code can find, then delete it
            // when the test is completed.
            string friendlyName = AppDomain.CurrentDomain.FriendlyName;
            string testReadMeFileName = friendlyName + ".ReadMe.txt";

            if (friendlyName.ToLower() != "Example1")
            {
                File.Copy("Example1.ReadMe.txt", testReadMeFileName);
            }

            try
            {
                using (var output = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
                {
                    string[] args = new string[] {"-SayWhat?"};
                    Example1.Main(args);
                    string spew = output.CapturedOutput;
                    Assert.IsTrue(spew.Contains(args[0]));
                    Assert.IsTrue(spew.Contains("Failed to process args:"));
                    Assert.IsTrue(spew.Contains("Example1 Usage:"));
                    Assert.IsTrue(spew.Contains("Example1 -SayHello:true"));
                }
            }
            finally
            {
                File.Delete(testReadMeFileName);
            }
        }
    }
}