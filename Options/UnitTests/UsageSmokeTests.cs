﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.IO;
using SharperHacks.Options.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharperHacks.Diagnostics;

namespace SharperHacks.Options.UnitTests
{
    /// <summary>
    /// UsageSmokeTests test class, drives test coverage of the Usage class to 100%.
    /// </summary>
    [TestClass]
    public class UsageSmokeTests
    {
        private static readonly string _shortDescription = "shortDescription";

        private static readonly string[] _longDescription = new string[]
        {
            "Long description line 1.",
            "Long description line 2.",
            "Long description line 4.",
        };

        private static readonly int _longDescriptionCount = _longDescription.Length;

        private static readonly string[] _examples = new string[]
        {
            "Example 1.",
            "Example 2.",
            "Example 3.",
        };

        private static readonly int _examplesCount = _examples.Length;

#if false
    [TestMethod]
    public void Constructor_KeyIUsageDocument()
    {
        string fakeName = "VerbOrOptionName";
        fileInfo fileInfo = CreateUsageFile
        IUsageDocument usageDocument = new UsageDocument(fileInfo);
        IUsage t1 = new Usage(fakeName, usageDocument);
    }
#endif
        private static readonly string _programPath = AppDomain.CurrentDomain.BaseDirectory;
        private static readonly string _programName = AppDomain.CurrentDomain.FriendlyName;

        // Array of default readme file names that Usage() will try in the order specified here.
        private static readonly string[] _fileNames =
        {
            Path.Combine(_programPath, _programName) + ".ReadMe",
            Path.Combine(_programPath, _programName) + ".ReadMe.txt",
            Path.Combine(_programPath, "ReadMe"),
            Path.Combine(_programPath, "ReadMe.txt"),
        };

        private static readonly string[] _readMeContent =
        {
            "TestReadMe",
            "Delete on sight!",
        };

        private void CleanUpFiles()
        {
            foreach (var fileName in _fileNames)
            {
                File.Delete(fileName);
            }
        }

        private void CreateTestReadMeFile(string fileName)
        {
            File.WriteAllLines(fileName, _readMeContent);
        }

        private void RunAssertionsForReadMeConstructors(bool emptyContentExpected, IUsage t1)
        {
//            Assert.IsTrue(t1.IsReadMe);

            string[] content = t1.All.ToArray();

            if (emptyContentExpected)
            {
                Assert.AreEqual(0, content.Length);
            }
            else
            {
                Assert.AreEqual(_readMeContent.Length, content.Length);
                Assert.IsTrue(_readMeContent.SequenceEqual(content));
            }
        }

        private void TestDefaultConstructor(bool emptyContentExpected)
        {
            IUsage t1 = new Usage();
            RunAssertionsForReadMeConstructors(emptyContentExpected, t1);
        }

        private void TestReadMeFileSuppliedConstructor(bool emptyContentExpected, FileInfo fi,
            UsageDocumentScope udt)
        {
            IUsage t1 = new Usage(fi, new UsageKeyScopePair("t1", udt));
            RunAssertionsForReadMeConstructors(emptyContentExpected, t1);
        }

        /// <summary>
        /// Smoke test <see cref="Usage()">Usage()</see>.
        /// </summary>
        [TestMethod]
        public void Constructor_DefaultAndReadMeFile()
        {
            // In case the test crashed on a previous run.
            CleanUpFiles();

            // Start by testing the missing file execution path
            TestDefaultConstructor(true);
            TestReadMeFileSuppliedConstructor(true, new FileInfo(_fileNames[0]), UsageDocumentScope.ApplicationReadMe);

            try
            {
                foreach (var fileName in _fileNames)
                {
                    CreateTestReadMeFile(fileName);
                    TestDefaultConstructor(false);
                    TestReadMeFileSuppliedConstructor(false, new FileInfo(fileName), UsageDocumentScope.ApplicationReadMe);
                    File.Delete(fileName);
                }
            }
            finally
            {
                CleanUpFiles();
            }
        }

        /// <summary>
        /// Smoke test <see cref="Usage(string)">Usage(string shortDescription)</see>.
        /// </summary>
        /// <remarks>
        ///  <list type="bullet">
        ///   <item>Verifies that <see cref="Usage.ShortDescription"/> is not null and matches input.</item>
        ///   <item>Verifies that <see cref="Usage.LongDescription"/> and <see cref="Usage.Examples"/> are not null.</item>
        ///   <item>Verifies that <see cref="Usage.LongDescription"/> and <see cref="Usage.Examples"/> LongDescription and Examples is empty.</item>
        ///  </list>
        /// </remarks>
        [TestMethod]
        public void Constructor_ShortDescription()
        {
            IUsage t1 = new Usage(_shortDescription, new UsageKeyScopePair("t1", UsageDocumentScope.ApplicationReadMe));
            Assert.IsNotNull(t1.ShortDescription);
            Assert.IsNotNull(t1.LongDescription);
            Assert.IsNotNull((t1.Examples));
            Assert.AreEqual(_shortDescription, t1.ShortDescription);
            Assert.AreEqual(0, t1.LongDescription.Count());
            Assert.AreEqual(0, t1.Examples.Count());
            Assert.AreEqual(UsageDocumentScope.ApplicationReadMe, t1.KeyScopePair.Scope);
//            Assert.IsFalse(t1.IsReadMe);
        }

        /// <summary>
        /// Smoke test <see cref="Usage(string, string[])">Usage(string shortDescription, string[] longDescription)</see>.
        /// </summary>
        /// <remarks>
        ///  <list type="bullet">
        ///   <item>Verifies that <see cref="Usage.ShortDescription"/> is not null and matches input.</item>
        ///   <item>Verifies that <see cref="Usage.LongDescription"/> is not null, item count and content match input.</item>
        ///   <item>Verifies that <see cref="Usage.Examples"/> is not null and is empty.</item>
        ///  </list>
        /// </remarks>
        [TestMethod]
        public void Constructor_ShortAndLongDescription()
        {
            IUsage t1 = new Usage(
                _shortDescription, 
                _longDescription, 
                new UsageKeyScopePair("t1", UsageDocumentScope.OptionReadMe));
            Assert.IsNotNull(t1.ShortDescription);
            Assert.IsNotNull(t1.LongDescription);
            Assert.IsNotNull((t1.Examples));
            Assert.AreEqual(_shortDescription, t1.ShortDescription);
            Assert.AreEqual(_longDescriptionCount, t1.LongDescription.Count());
            Assert.AreEqual(0, t1.Examples.Count());

            Assert.AreEqual(UsageDocumentScope.OptionReadMe, t1.KeyScopePair.Scope);
//            Assert.IsFalse(t1.IsReadMe);

            int idx = 0;
            foreach (var str in t1.LongDescription)
            {
                Assert.AreEqual(_longDescription[idx], str);
                idx++;
            }
        }

        /// <summary>
        /// Smoke test <see cref="Usage(string, string[], string[])">Usage(string shortDescription, string[] longDescription, string[] examples)</see>
        /// </summary>
        /// <remarks>
        ///  <list type="bullet">
        ///   <item>Verifies that <see cref="Usage.ShortDescription"/> is not null and matches input.</item>
        ///   <item>Verifies that <see cref="Usage.LongDescription"/> is not null, item count and content match input.</item>
        ///   <item>Verifies that <see cref="Usage.Examples"/> is not null, item count and content match input.</item>
        ///  </list>
        /// </remarks>
        [TestMethod]
        public void Constructor_ShortAndLongDescriptionAndExamples()
        {
            IUsage t1 = new Usage(
                _shortDescription, 
                _longDescription, 
                _examples, 
                new UsageKeyScopePair("t1", UsageDocumentScope.VerbSource)
                );
            Assert.IsNotNull(t1.ShortDescription);
            Assert.IsNotNull(t1.LongDescription);
            Assert.IsNotNull((t1.Examples));
            Assert.AreEqual(_shortDescription, t1.ShortDescription);
            Assert.AreEqual(_longDescriptionCount, t1.LongDescription.Count());
            Assert.AreEqual(_examplesCount, t1.Examples.Count());

            Assert.AreEqual(UsageDocumentScope.VerbSource, t1.KeyScopePair.Scope);

            var lines = new List<string> {t1.ShortDescription};

            int idx = 0;
            foreach (var str in t1.LongDescription)
            {
                Assert.AreEqual(_longDescription[idx], str);
                idx++;
                lines.Add(str);
            }

            idx = 0;
            foreach (var str in t1.Examples)
            {
                Assert.AreEqual(_examples[idx], str);
                idx++;
                lines.Add(str);
            }

            // We do this twice in order to test return short-cut in EnumerateAll().
            Assert.IsTrue(t1.All.SequenceEqual(lines));
            Assert.IsTrue(t1.All.SequenceEqual(lines));
        }

        /// <summary>
        /// Verify that Usage all usage constructors throw VerifyException when shortDescription argument is null.
        /// </summary>
        /// <remarks>
        ///  Invokes the following constructors:
        ///  <list type="bullet">
        ///   <see cref="Usage(string)"/>
        ///   <see cref="Usage(string, string[])"/>
        ///   <see cref="Usage(string, string[], string[])"/>
        ///  </list>
        ///  Verifying that they all throw VerifyException when the shortDescription parameter is null.
        /// </remarks>
        [TestMethod]
        public void NullShortDescriptionThrowsVerifyError()
        {
            bool exceptionCaught = false;
            try
            {
                // ReSharper disable once UnusedVariable
                Usage t1 = new Usage(null);
            } // This line intentionally never reached.
            catch (VerifyException)
            {
                exceptionCaught = true;
            }

            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            try
            {
                // ReSharper disable once UnusedVariable
                Usage t2 = new Usage(null, _longDescription, new UsageKeyScopePair("t2", UsageDocumentScope.VerbReadMe));
            } // This line intentionally never reached.
            catch (VerifyException)
            {
                exceptionCaught = true;
            }

            Assert.IsTrue(exceptionCaught);
            exceptionCaught = false;

            try
            {
                // ReSharper disable once UnusedVariable
                Usage t3 = new Usage(null, _longDescription, _examples, new UsageKeyScopePair("t3", UsageDocumentScope.VerbReadMe));
            } // This line intentionally never reached.
            catch (VerifyException)
            {
                exceptionCaught = true;
            }

            Assert.IsTrue(exceptionCaught);
        }

        /// <summary>
        /// Verify that Usage constructors handle null longDescription argument gracefully.
        /// </summary>
        /// <remarks>
        ///  Invokes the following constructors with null longDescription argument:
        ///  <list type="bullet">
        ///   <see cref="Usage(string, string[])"/>
        ///   <see cref="Usage(string, string[], string[])"/>
        ///  </list>
        ///  Verifying that <see cref="Usage.LongDescription"/> is not null and is empty.
        /// </remarks>
        [TestMethod]
        public void NullLongDescriptionIsSafe()
        {
            Usage t1 = new Usage(_shortDescription, null, new UsageKeyScopePair("t1", UsageDocumentScope.VerbReadMe));
            Assert.AreEqual(_shortDescription, t1.ShortDescription);
            Assert.IsNotNull(t1.LongDescription);
            Assert.AreEqual(0, t1.LongDescription.Count());

            Usage t2 = new Usage(_shortDescription, null, _examples, new UsageKeyScopePair("t2", UsageDocumentScope.ApplicationReadMe));
            Assert.AreEqual(_shortDescription, t2.ShortDescription);
            Assert.IsNotNull(t2.LongDescription);
            Assert.AreEqual(0, t2.LongDescription.Count());
            Assert.AreEqual(_examples, t2.Examples);
        }

        /// <summary>
        /// Verify that Usage constructors handle null examples argument gracefully.
        /// </summary>
        /// <remarks>
        ///  Invokes the following constructors:
        ///  <list type="bullet">
        ///   <see cref="Usage(string, string[], string[])"/>
        ///  </list>
        ///  Verifying that <see cref="Usage.Examples"/> is not null and is empty.
        /// </remarks>
        [TestMethod]
        public void NullExamplesIsSafe()
        {
            Usage t1 = new Usage(
                _shortDescription, 
                _longDescription, 
                null, 
                new UsageKeyScopePair("t1", UsageDocumentScope.ApplicationReadMe)
                );
            Assert.AreEqual(_shortDescription, t1.ShortDescription);
            Assert.AreEqual(_longDescription, t1.LongDescription);
            Assert.IsNotNull(t1.Examples);
            Assert.AreEqual(0, t1.Examples.Count());
        }

        private const string _verb1Opt1Name = "verb1Opt1";
        private const string _verb1Opt2Name = "verb1Opt2";
        private const string _verb1Opt3Name = "verb1Opt3";

        private static readonly string[] _verb1Opt1Aliases = {"v1Opt1Aliase1", "v1Opt1Aliase2"};
        private static readonly string[] _verb1Opt2Aliases = {"v1Opt2Aliase1", "v1Opt2Aliase2" };
        private static readonly string[] _verb1Opt3Aliases = {"v1Opt2Aliase1", "v1Opt3Aliase2" };

        private const string _verb2Opt1Name = "verb2Opt1";
        private const string _verb2Opt2Name = "verb2Opt2";
        private const string _verb2Opt3Name = "verb2Opt3";

        private const string _verb3Opt1Name = "verb3Opt1";
        private const string _verb3Opt2Name = "verb3Opt2";
        private const string _verb3Opt3Name = "verb3Opt3";

        // TODO: Next!
        private class TestVerb1 : VerbBase
        {
            public VerbOption<int> Option1 = 
                new VerbOption<int>(
                    true, 
                    _verb1Opt1Name, 
                    _verb1Opt1Aliases, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb1Opt1Name, UsageDocumentScope.ApplicationReadMe)
                        )
                    );
            public VerbOption<int> Option2 = 
                new VerbOption<int>(
                    true, 
                    _verb1Opt2Name, 
                    _verb1Opt1Aliases, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb1Opt2Name, UsageDocumentScope.ApplicationReadMe)
                    )
                );
            public VerbOption<int> Option3 = 
                new VerbOption<int>(
                    true, 
                    _verb1Opt3Name, 
                    _verb1Opt1Aliases, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb1Opt3Name, UsageDocumentScope.ApplicationReadMe)
                    )
                );

            public TestVerb1(string name) : 
                base(
                    name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(name, UsageDocumentScope.ApplicationReadMe)
                        )
                    ) { }

            public override int Run() => 0;
        }

        private class TestVerb2 : VerbBase
        {
            public VerbOption<int> Option1 = 
                new VerbOption<int>(
                    true, 
                    _verb2Opt1Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb2Opt1Name, UsageDocumentScope.OptionReadMe)
                        )
                    );

            public VerbOption<int> Option2 = 
                new VerbOption<int>(
                    true, 
                    _verb2Opt2Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb2Opt2Name, UsageDocumentScope.OptionReadMe)
                    )
                );

            public VerbOption<int> Option3 = 
                new VerbOption<int>(
                    true, 
                    _verb2Opt3Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb2Opt3Name, UsageDocumentScope.OptionReadMe)
                    )
                );

            public TestVerb2(string name) : 
                base(
                    name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(name, UsageDocumentScope.OptionReadMe)
                        )
                    ) { }

            public override int Run() => 0;
        }

        private class TestVerb3 : VerbBase
        {
            public VerbOption<int> Option1 = 
                new VerbOption<int>(
                    true, 
                    _verb3Opt1Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb3Opt1Name, UsageDocumentScope.VerbReadMe)
                        )
                    );

            public VerbOption<int> Option2 = 
                new VerbOption<int>(
                    true, 
                    _verb3Opt2Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb3Opt2Name, UsageDocumentScope.VerbReadMe)
                    )
                );

            public VerbOption<int> Option3 = 
                new VerbOption<int>(
                    true, 
                    _verb3Opt3Name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(_verb3Opt3Name, UsageDocumentScope.VerbReadMe)
                    )
                );

            
            public TestVerb3(string name) : 
                base(
                    name, 
                    new Usage(
                        _shortDescription, 
                        _longDescription, 
                        _examples, 
                        new UsageKeyScopePair(name, UsageDocumentScope.VerbReadMe)
                        )
                    ) { }

            public override int Run() => 0;
        }

        private const string _verb1Name = "verb1";
        private const string _verb2Name = "verb2";
        private const string _verb3Name = "verb3";

        /// <summary>
        /// Smoke test <see cref="Usage.Write(IUsage)"/> and <see cref="Usage.Write(TextWriter, IUsage)"/>.
        /// The later is called by the former, so we only need the one call here.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [TestMethod]
        public void StaticWrite()
        {
            OptionsDb.Reset();
            // ReSharper disable once UnusedVariable
            var help = new Option<bool>(
                true,
                "help",
                new [] {"?", "h"},
                new Usage("Show the help/usage content.",
                    new UsageKeyScopePair("help", UsageDocumentScope.OptionReadMe)
                    )
                );

            // ReSharper disable once UnusedVariable
            var v1 = new TestVerb1(_verb1Name);
            // ReSharper disable once UnusedVariable
            var v2 = new TestVerb2(_verb2Name);
            // ReSharper disable once UnusedVariable
            var v3 = new TestVerb3(_verb3Name);

            var testShortDescription = "Test short description";
            Usage programUsage = new Usage(
                testShortDescription, 
                _longDescription, 
                new UsageKeyScopePair("Test", UsageDocumentScope.ApplicationReadMe));
            string usageOut;

            using (var captured = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
            {
                // Capture the output.
                Usage.Write(programUsage);
                usageOut = captured.CapturedOutput;
            }

            // Verify that the output contains all of our data.
            Assert.IsTrue(usageOut.Contains("help"));
            Assert.IsTrue(usageOut.Contains(testShortDescription));

            Assert.IsTrue(usageOut.Contains(_verb1Name));
            Assert.IsTrue(usageOut.Contains(_verb1Opt1Name));
            Assert.IsTrue(usageOut.Contains(_verb1Opt2Name));
            Assert.IsTrue(usageOut.Contains(_verb1Opt3Name));

            Assert.IsTrue(usageOut.Contains(_verb2Name));
            Assert.IsTrue(usageOut.Contains(_verb2Opt1Name));
            Assert.IsTrue(usageOut.Contains(_verb2Opt2Name));
            Assert.IsTrue(usageOut.Contains(_verb2Opt3Name));

            Assert.IsTrue(usageOut.Contains(_verb3Name));
            Assert.IsTrue(usageOut.Contains(_verb3Opt1Name));
            Assert.IsTrue(usageOut.Contains(_verb3Opt2Name));
            Assert.IsTrue(usageOut.Contains(_verb3Opt3Name));

            Assert.IsTrue(usageOut.Contains(_shortDescription));

            foreach (var item in _longDescription)
            {
                Assert.IsTrue(usageOut.Contains(item));
            }

            foreach (var item in _examples)
            {
                Assert.IsTrue(usageOut.Contains(item));
            }

            OptionsDb.Reset(); // We added Verbs and TestOptions, don't pollute other unit test results.
        }

        [TestMethod]
        public void Write()
        {
            CreateTestReadMeFile(_fileNames[1]);

            Usage u1 = new Usage();

            // Redirect console output so we can capture it.
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);

            u1.Write(Console.Out);

            CleanUpFiles();
        }
    }
}