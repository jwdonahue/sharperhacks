﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.IO;
using SharperHacks.Options;
using System.Linq;
using SharperHacks.Options.Interfaces;
using SharperHacks.Options.UnitTests;

namespace SharperHacks.Options.UnitTests
{
    [TestClass]
    public class UsageErrorHandlerSmokeTest
    {
        private static readonly string[] _unmatched =
        {
            "Unmatched1",
            "Unmatched2",
            "Unmatched3",
        };

        private static readonly string[] _errors =
        {
            "Error1",
            "Error2",
            "Error3",
        };

        [TestMethod]
        public void DefaultConstructor()
        {
            using (var output = new CaptureConsoleOutput((new TimeSpan(0, 0, 0, 60)).Milliseconds))
            {
                IUsageErrorHandler handler = new UsageErrorHandler(null, null);
                foreach (var item in _unmatched)
                {
                    handler.AddUnmatched(item);
                }

                foreach (var item in _errors)
                {
                    handler.AddError(item);
                }

                Assert.IsTrue(handler.HasFailed);
                Assert.IsTrue(handler.HasUnmatched);
                Assert.IsTrue(handler.HasParseErrors);

                handler.Invoke();

                foreach (var t in _unmatched)
                {
                    Assert.IsTrue(handler.Unmatched.Contains(t));
                    Assert.IsTrue(output.CapturedOutput.Contains(t));
                }

                foreach (var t in _errors)
                {
                    Assert.IsTrue(handler.ParseErrors.Contains(t));
                    Assert.IsTrue(output.CapturedOutput.Contains(t));
                }

            }
        }
    }
}