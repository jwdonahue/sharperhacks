﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.UnitTests
{
    public class VerbWithOptions : VerbBase
    {
        public VerbOption<int> IntOption = new VerbOption<int>(true, "IntOption");
        public VerbOption<bool> BoolOption = new VerbOption<bool>(true, "BoolOption", new Usage());
        public VerbOption<string> StringOption = new VerbOption<string>(true, "StringOption", "New Default", new Usage());
        public VerbOption<double> DoublOptionWithAliases = new VerbOption<double>(
            true, 
            "DoubleOptionWithAliases", 
            new [] {"DOWA"}, 
            new Usage());
        public VerbOption<char> CharOptionWithAliasesAndDefault = new VerbOption<char>(
            true,
            "CharOptionWithAliasesAndDefault",
            new [] {"COWAAD"},
            'c',
            new Usage());

        public int RunResult { get; set; }

        public VerbWithOptions(string name, IUsage usage, int runResult) : base(name, usage) 
        { 
            RunResult = runResult;
        }

        public override int Run() => RunResult;        
    }

}
