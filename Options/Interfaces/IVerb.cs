﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Collections.Generic;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// Interface for accessing a verb's name, usage and options.
    /// </summary>
    public interface IVerb
    {
        /// <summary>
        /// Get whether the verb instance is a singleton.
        /// </summary>
        bool IsSingleton { get; }

        /// <summary>
        /// The name of the option or verb.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The verb's usage information.
        /// </summary>
        IUsage Usage { get; }

        /// <summary>
        /// The verb's supported options, if any, or an empty IEnumerable&lt;IOption&gt;.
        /// </summary>
        IEnumerable<IOption> Options { get; }

        /// <summary>
        /// Gets the named option, if it exist, or returns null.
        /// </summary>
        /// <param name="optionName"></param>
        /// <returns>The IOption named by optionName, or null.</returns>
        IOption GetOption(string optionName);

        /// <summary>
        /// Run the code that implements the verb behavior.
        /// </summary>
        /// <returns>int</returns>
        int Run();

        /// <summary>
        /// Spawn the derived type.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// <para>Must not return null if IsSingleton==false.</para>
        /// <para>Not a clone of the object.  It must be a new instance of the object type.</para>
        /// </remarks>
        IVerb Spawn();
    }
}