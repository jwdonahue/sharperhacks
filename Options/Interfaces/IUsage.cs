﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// Interface for accessing verb usage data.
    /// </summary>
    public interface IUsage
    {
        /// <summary>
        /// When true, indicates that there is only a single text blob and the other
        /// fields will be null.
        /// When false, separate Short/Long descriptions and examples may be available.
        /// </summary>
        bool IsReadMe { get; }

        /// <summary>
        /// The key (option or verb name) and Scope.
        /// </summary>
        UsageKeyScopePair KeyScopePair { get; }

        /// <summary>
        /// A short (one-liner) verb/option description.
        /// Briefly answers "what is it?" or "what does it do?".
        /// </summary>
        string ShortDescription { get; }

        /// <summary>
        /// A long (complete/multi-line) verb/option description.
        /// </summary>
        IEnumerable<string> LongDescription { get; }

        /// <summary>
        /// Usage examples.
        /// </summary>
        IEnumerable<string> Examples { get; }

        /// <summary>
        /// Short and long descriptions, and examples.
        /// </summary>
        IEnumerable<string> All { get; }

        /// <summary>
        /// Write contents to the provided TextWriter
        /// </summary>
        /// <param name="tw"></param>
        void Write(TextWriter tw);
    }
}