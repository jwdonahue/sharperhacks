﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// IStandardOptions is used internally, but may also be used by anyone
    /// wishing to roll their own components to interface with out code.
    /// </summary>
    public interface IStandardOptions
    {
        /// <summary>
        /// Get whether there is a StandardHelp option available.
        /// </summary>
        bool HasStandardHelpOption { get; }

        /// <summary>
        /// Get the IOption reference to the StandardHelp option.
        /// </summary>
        /// <remarks>
        /// If HasStandardHelpOption is true, this will not be null,
        /// if false, it will be null.
        /// </remarks>
        IOption<bool> StandardHelp { get; }

        /// <summary>
        /// Get whether there is a StandardDiagnostic option available.
        /// </summary>
        bool HasStandardDiagnosticsOption { get; }

        /// <summary>
        /// Get the IOption reference to the StandardDiagnostic option.
        /// </summary>
        /// <remarks>
        /// If HasStandardDiagnosticsOption is true, this will not be null,
        /// if false, it will be null.
        /// </remarks>
        IOption<string> StandardDiagnostic { get; }
    }
}