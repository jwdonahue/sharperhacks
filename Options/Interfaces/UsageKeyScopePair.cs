﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// WIP
    /// </summary>
    public class UsageKeyScopePair : IEquatable<UsageKeyScopePair>
    {
        /// <summary>
        /// Get the usage key.
        /// This should be the option, verb or application name.
        /// </summary>
        public string Key { get; }

        /// <summary>
        /// Get the scope.
        /// </summary>
        public UsageDocumentScope Scope { get; }

        /// <summary>
        /// Determines the equality of an object reference to a UsageKeyScopePair,
        /// with the current UsageKeyScopePair.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }

            return Equals((UsageKeyScopePair) obj);
        }

        /// <summary>
        /// Determines the equality of a UsageKeyScopePair to the current one.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public virtual bool Equals(UsageKeyScopePair other)
        {
            if (object.ReferenceEquals(null, other)) // (null == other)
            {
                return false;
            }

            return (other.Key == Key) && (other.Scope == Scope);
        }

        /// <inheritdoc cref="System.Object.GetHashCode"/>
        public override int GetHashCode()
        {
            return ShiftAndWrap(Key.GetHashCode(), 2) ^ Scope.GetHashCode();
        }

        /// <summary>
        /// Compare UsageKeyScopePair objects for equality.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator ==(UsageKeyScopePair left, UsageKeyScopePair right)
        {
            return object.ReferenceEquals(null, left) ? object.ReferenceEquals(null, right) : left.Equals(right);
        }

        /// <summary>
        /// Compare UsageKeyScopePair objects for non-equality.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static bool operator !=(UsageKeyScopePair left, UsageKeyScopePair right)
        {
            return !(left == right);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="scope"></param>
        public UsageKeyScopePair(string key, UsageDocumentScope scope)
        {
            Key = key;
            Scope = scope;
        }

        // TODO: This belongs someplace where it will be more usable.
        private static int ShiftAndWrap(int value, int positions)
        {
            positions = positions & 0x1F;
  
            // Save the existing bit pattern, but interpret it as an unsigned integer.
            uint number = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
            // Preserve the bits to be discarded.
            uint wrapped = number >> (32 - positions);
            // Shift and wrap the discarded bits.
            return BitConverter.ToInt32(BitConverter.GetBytes((number << positions) | wrapped), 0);
        }        
    }
}