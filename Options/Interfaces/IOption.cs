﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// Interface for accessing program option data.
    /// </summary>
    public interface IOption
    {
        /// <summary>
        /// Aliases for the option, not including the name.
        /// </summary>
        IEnumerable<string> Aliases { get; }

        // TODO: GetHashCode based on Name?
        // TODO: If not, make sure that multiple OptName objects get updated by the parser!\
        // JwD: I believe the most recent one wins with the current code set.

        /// <summary>
        /// A unique object identifier.
        /// </summary>
        int GetHashCode();

#if false // TODO: Would this ever be useful?
        /// <summary>
        /// Get the option type.
        /// Use to cast to IOption&lt;T&gt;
        /// </summary>
        Type GetOptionType();
#endif
        /// <summary>
        /// Indicates whether the option is optional (true) or required (false)
        /// </summary>
        bool IsOptional { get; }

        /// <summary>
        /// The option name.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Sets the ValueString.
        /// </summary>
        /// <param name="valueRep">A string representation of the option value.</param>
        /// <returns></returns>
        bool SetValue(string valueRep);

        /// <summary>
        /// An IUsage describing the option and example uses.
        /// </summary>
        IUsage Usage { get; }

        /// <summary>
        /// A string representation of the option value.
        /// </summary>
        string ValueString { get; }

        //// TODO: Add DefaultValueString.
    }

    /// <summary>
    /// Interface for accessing typed program option data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IOption<out T> : IOption
    {
        /// <summary>
        /// Get the default value.
        /// </summary>
        T DefaultValue { get; }

        /// <summary>
        /// Get the current option value.
        /// </summary>
        T Value { get; }
    }
}