﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Collections.Generic;
using System.IO;

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// Method type that is called on parse errors.
    /// </summary>
    /// <param name="unmatched"></param>
    /// <param name="errors"></param>
    /// <param name="output"></param>
    /// TODO: Find a better home for delegate types?
    public delegate void UsageErrorHandlerDelegate(
        IEnumerable<string> unmatched,
        IEnumerable<string> errors,
        TextWriter output);

    /// <summary>
    /// Encapsulates unmatched and parser error handling.
    /// </summary>
    /// <remarks>
    /// Concrete visitors implement this interface to provide
    /// unmatched and parser error handling.  
    /// </remarks>
    public interface IUsageErrorHandler
    {
        /// <summary>
        /// Get whether there were any unmatched arguments, or parse/conversion errors.
        /// </summary>
        bool HasFailed { get; }

        /// <summary>
        /// Get whether there were any unmatched arguments.
        /// </summary>
        bool HasUnmatched { get; }

        /// <summary>
        /// Get whether there were any parse/conversion errors.
        /// </summary>
        bool HasParseErrors { get; }

        /// <summary>
        /// Accumulates unmatched arguments.
        /// </summary>
        /// <param name="item"></param>
        void AddUnmatched(string item);

        /// <summary>
        /// Accumulates error messages.
        /// </summary>
        /// <param name="item"></param>
        void AddError(string item);

        /// <summary>
        /// Accumulated unmatched arguments.
        /// </summary>
        IEnumerable<string> Unmatched { get; }

        /// <summary>
        /// Accumulated parse errors.
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> ParseErrors { get; }

        /// <summary>
        /// Invokes the error handler with the currently configured output (usually Console.Out).
        /// </summary>
        void Invoke();
#if false
        /// <summary>
        /// Invokes error handler with the specified usage file and document scope.
        /// </summary>
        /// <param name="usageFile"></param>
        /// <param name="scope"></param>
        void Invoke(FileInfo usageFile, UsageDocumentScope scope);
#endif
        /// <summary>
        /// Invokes the error handler with the specified output.
        /// </summary>
        /// <param name="tw"></param>
        void Invoke(TextWriter tw);

        /// <summary>
        /// Reset the underlying instance.  This is primarily for testing,
        /// but probably has other use cases.
        /// </summary>
        void Reset();
    }
}