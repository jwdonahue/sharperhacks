﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------
#if false
using System;

namespace SharperHacks.Options.Interfaces
{
    public interface IDiagnosticOption
    {
        // TODO: Make a commitment to a specific diagnostic implemenation?

        string DefaultLevel { get; }

        string Level { get; }

        object GetLogger();

        Type GetLoggerType();
    }
}
#endif