﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

namespace SharperHacks.Options.Interfaces
{
    /// <summary>
    /// Represents the scope of the usage document.
    /// </summary>
    public enum UsageDocumentScope
    {
        /// <summary>
        /// Document is plain text readme file for entire application.
        /// </summary>
        ApplicationReadMe,

        /// <summary>
        /// Document is application source code strings.
        /// </summary>
        ApplicationSource,

        /// <summary>
        /// Document is plain text readme file for just one option.
        /// </summary>
        OptionReadMe,

        /// <summary>
        /// Document is option source code strings.
        /// </summary>
        OptionSource,

        /// <summary>
        /// Document is plain text readme file for just one verb.
        /// </summary>
        VerbReadMe,

        /// <summary>
        /// Document is verb source code strings.
        /// </summary>
        VerbSource,

        /// <summary>
        /// Document is plain text readme file for this verb option.
        /// </summary>
        VerbOptionReadMe,

        /// <summary>
        /// Document is verb option source code strings.
        /// </summary>
        VerbOptionSource,
    };
}