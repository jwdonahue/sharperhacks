﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

//
// Simple example of verb usage in a program.
//

using System;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.Examples
{
    // Derive a VerbOption<> container class from VerbBase.
    class SayHelloVerb : VerbBase
    {
        private VerbOption<string> To = new VerbOption<string>(
            false,
            "To",
            new Usage(
                "The name of the person to say hello to.",
                new UsageKeyScopePair("To", UsageDocumentScope.VerbOptionSource)
            )
        );

        public SayHelloVerb()
            : base(
                "SayHello",
                new Usage(
                    "Say hello.",
                    new[] {"Say hello, optionally to the name provided by the 'To' parameter."},
                    new[] {"SayHello -To:Dave"},
                    new UsageKeyScopePair("SayHello", UsageDocumentScope.VerbSource))
            )
        {}

        public override int Run()
        {
            string outString = "Hello";

            if (To.Value != string.Empty)
            {
                outString += ' ';
                outString += To.Value;
            }

            Console.WriteLine(outString);

            return 0;
        }
    }

    class Program : VerbsBase
    {
        private SayHelloVerb verb1 = new SayHelloVerb();

        public Program(string[] args) : 
            base( 
                args,
                new UsageErrorHandler(
                    new Usage(
                        "Try 'Example2 SayHello -To:somebody'",
                        new UsageKeyScopePair(
                            "Application", 
                            UsageDocumentScope.ApplicationSource)),
                    null),
                true,  // Verbs required.
                false, // Only one verb allowed.
                false) // Fail on verb failure.
        {}
    }

    public class Example2
    {
        public static int Main(string[] args)
        {
            return (new Program(args).RunIfParsedSuccessfully());
        }
    }
}