﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

//
// Simple example of option usage in a program.
//
using System;

namespace SharperHacks.Options.Examples
{
    // Derive a  class from OptionsBase.
    // The OptionsBase constructor consists of boiler plate code that
    // reduces the complexity of your Main() method.
    class Example1App : OptionsBase
    {
        // The Option<> constructor will register the SayHello option
        // with the OptionsDB.
        //
        // Note that any Option<>, in any  module that is initialized prior to
        // calling an OptionsBase constructor, will be processed at the time
        // of the base constructor call.  It is not strictly required that the
        // options reside in this class or file.
        public Option<bool> Flag1 = new Option<bool>(true, "SayHello");
        
        // The base constructor will call OptionsParser.UpdateOptionsFromArguments(),
        // which will in turn, update the value of Flag1 if SayHello is among the args.
        public Example1App(string[] args) : base(args, new UsageErrorHandler()) {}

        /// <summary>
        /// Implements the guts of our simple Hello World app.
        /// </summary>
        /// <returns>0</returns>
        /// <remarcks>
        /// Called from OptionsBase.RunIfParsedSuccessfully()
        /// </remarcks>
        protected override int Run()
        {
            if (Flag1.Value)
            {
                Console.WriteLine("Hello World!");
            }

            return 0;
        }
    }

    /// <summary>
    /// Example1 implementation.
    /// </summary>
    public class Example1
    {
        /// <summary>
        /// Initialize options, then update from args. If args not
        /// parse-able, display the readme file.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int Main(string[] args)
        {
            return (new Example1App(args)).RunIfParsedSuccessfully();
        }
    }
}
