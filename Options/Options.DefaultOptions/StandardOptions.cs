﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Threading;

using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.StandardOptions
{
    public class StandardOptions : IStandardOptions
    {
        #region IStandardOptions
        ///<inheritdoc cref="IStandardOptions.HasStandardHelpOption"/>
        public bool HasStandardHelpOption => null != StandardHelp;

        /// <inheritdoc cref="IStandardOptions.StandardHelp"/>
        public IOption<bool> StandardHelp => _standardHelp.Help;

        /// <inheritdoc cref="IStandardOptions.HasStandardDiagnostics"/>
        public bool HasStandardDiagnosticsOption => null != StandardDiagnostic;

        ///<inheritdoc cref="IStandardOptions.StandardDiagnostic"/>
        public IOption<string> StandardDiagnostic => _standardDiagnosticOption;
        #endregion IStandardOptions

        #region Public Factories
#if false
        /// <summary>
        /// StandardOptions factory
        /// </summary>
        /// <returns>True if StandardOption created, false if they've already been created.</returns>
        public static bool CreateDefaultStaticStandardOptions()
        {
            return SetTheStandardOptions(new StandardOptions());
        }
#endif
        #endregion

        #region Public static accessors

        /// <summary>
        /// Reset the internal instance counter.
        /// </summary>
        /// <remarks>
        /// Primarily for unit test purposes, but might have other uses.
        /// </remarks>
        public static void Reset()
        {
            Interlocked.Exchange(ref _instanceCounter, 0);
        }

        #endregion Public static accessors

        #region Public Constructors

        /// <summary>
        /// Default constructor creates all of the standard options.
        /// </summary>
        /// <param name="throwIfNotFirst">
        /// Defaults to true, set to false for unit testing, where needed.
        /// </param>
        public StandardOptions(bool throwIfNotFirst = true)
        {
// TODO:            _standardDiagnosticOption = new DiagnosticOption();
            Interlocked.Increment(ref _instanceCounter);
            if (throwIfNotFirst)
            {
                Verify.IsTrue(Interlocked.Read(ref _instanceCounter) == 1);
            }

            _standardHelp = new StandardHelp(throwIfNotFirst);

        }

        #endregion Public Constructors

        #region Private

        private static long _instanceCounter = 0;

        private IOption<string> _standardDiagnosticOption = null;

        private readonly StandardHelp _standardHelp = null;

        #endregion Private
    }
}