﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System.Threading;
using SharperHacks.Diagnostics.Constraints;
using SharperHacks.Options.Interfaces;

namespace SharperHacks.Options.StandardOptions
{
    public class StandardHelp
    {
        // TODO: Make this a singleton?

        public static string DefaultShortDescription { get; } = "Show usage.";

        public static string[] DefaultLongDescription { get; } = null;

        public string ShortDescription { get; } = DefaultShortDescription;

        public string[] LongDescription { get; } = DefaultLongDescription;

        /// <summary>
        /// Get the Help option.
        /// </summary>
        public IOption<bool> Help { get; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Assumes there's a readme file. TODO: pointer to readme file selection algorithm.
        /// </para>
        /// </remarks>
        public StandardHelp(bool throwIfNotFirst = true)
        {
            Interlocked.Increment(ref _instances);

            if (throwIfNotFirst)
            {
                Verify.IsTrue(Interlocked.Read(ref _instances) == 1);
            }

            var usage = new Usage();
            Help = Option<bool>.CreateOption(
                "Help",
                new[] {"H", "?"},
                true,
                ShortDescription,
                LongDescription
            );
        }

        private static long _instances = 0;
    }
}
