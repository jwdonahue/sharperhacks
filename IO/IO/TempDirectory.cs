﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using SharperHacks.Diagnostics.Constraints;
using System;
using System.IO;

namespace SharperHacks.IO
{
    /// <summary>
    /// Manages lifetime of temporary directories with Path.GetTempPath() at their root.
    /// </summary>
    /// <remarks>
    /// Each instance has a unique name and will be deleted when the object is disposed.
    /// Each instance has Path.GetTempPath() as it's root.
    /// </remarks>
    public class TempDirectory : IDisposable
    {
        /// <summary>
        /// The DirectoryInfo object obtained on successful directory creation.
        /// </summary>
        public DirectoryInfo DirectoryInfo { get; private set; }

        #region IDisposable

        private bool _disposedValue; // To detect redundant calls

        /// <summary>
        /// Implements the dispose pattern.
        /// </summary>
        /// <param name="disposing">
        /// Currently ignored. No managed objects to dispose.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (null != DirectoryInfo)
                {
                    if (disposing && Directory.Exists(DirectoryInfo.FullName))
                    {
                        Directory.Delete(DirectoryInfo.FullName, true);
                    }
                    DirectoryInfo = null;
                }
                _disposedValue = true;
            }
        }

        /// <summary>
        /// Finalizer implements the Dispose pattern.
        /// </summary>
        ~TempDirectory()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.

            // TODO: Add trace spew for finalize before dispose.
            //       Throwing from a finalizer is a bad idea.

            // Potential performance hit ensues if finalizer called before Dispose(),
            // but we do it anyway.
            Dispose(!_disposedValue);
        }

        /// <summary>
        /// Implements the Dispose pattern.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // Because we override the finalizer.
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable

        #region Constructors
        /// <summary>
        /// Default constructor creates a new GUID string that is used to create
        /// a directory in the Path.GetTempPath() directory.
        /// </summary>
        public TempDirectory()
        {
            CreateDirectory("");
        }

        /// <summary>
        /// Constructor uses the prefix and a new GUID string to create a directory
        /// in the Path.GetTempPath() directory.
        /// </summary>
        /// <param name="prefix"></param>
        public TempDirectory(string prefix)
        {
            Verify.IsNotNull(prefix);
            CreateDirectory(prefix);
        }
        #endregion Constructors

        #region Public methods

        /// <summary>
        /// Creates a sub-directory of the given name.
        /// </summary>
        /// <param name="name">Name of the new subdirectory.</param>
        /// <returns>New DirectoryInfo object.</returns>
        public DirectoryInfo CreateNamedSubdirectory(string name)
        {
            return DirectoryInfo.CreateSubdirectory(name);
        }

        /// <summary>
        /// Creates a sub-directory using a new GUID string.
        /// </summary>
        /// <returns>New DirectoryInfo object.</returns>
        public DirectoryInfo CreateSubdirectory()
        {
            return DirectoryInfo.CreateSubdirectory(Guid.NewGuid().ToString());
        }

        /// <summary>
        /// Creates a sub-directory using the prefix and a new GUID string.
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns>New DirectoryInfo object.</returns>
        public DirectoryInfo CreateSubdirectory(string prefix)
        {
            return DirectoryInfo.CreateSubdirectory(prefix + Guid.NewGuid().ToString());
        }
        #endregion Public methods

        #region Private Methods

        private void CreateDirectory(string prefix)
        {
            Verify.IsFalse(prefix.Contains(".."));

            string path;

            // We loop until we create a path that does not yet exist.
            // In the life time of this code, somewhere in the universe, this loop
            // might iterate more than once, however unlikely, this loop is required.
            do
            {
                path = Path.Combine(Path.GetTempPath(), prefix + Guid.NewGuid().ToString());
            } while (Directory.Exists(path));

            DirectoryInfo = Directory.CreateDirectory(path);
        }
        #endregion
    }
}
