﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.IO;
using System.Linq;
using System.Text;
using SharperHacks.Diagnostics.Constraints;

namespace SharperHacks
{
    namespace IO
    {
        /// <summary>
        /// Helper class for managing temporary files.
        /// </summary>
        /// <remarks>
        /// After creating the file, the FileStream is left open to
        /// hold the file.  
        /// </remarks>
        public class TempFile : IDisposable
        {
            /// <summary>
            /// The FileInfo object obtained on successful file creation.
            /// </summary>
            public FileInfo FileInfo { get; private set; }

            /// <summary>
            /// The FileStream resulting from a successful file creation.
            /// </summary>
            public FileStream FileStream { get; private set; }

            #region IDisposable Support
            private bool _disposedValue; // To detect redundant calls

            /// <summary>
            /// Implements the dispose pattern.
            /// </summary>
            /// <param name="disposing">
            /// Disposes FileStream and deletes the file.
            /// </param>
            protected virtual void Dispose(bool disposing)
            {
                if (!_disposedValue)
                {
                    _disposedValue = true;

                    if (disposing)
                    {
                        FileStream.Dispose();
                        try
                        {
                            File.Delete(FileInfo.FullName);
                        }
                        finally
                        {
                            FileStream = null;
                            FileInfo = null;
                        }
                    }
                }
            }

            /// <summary>
            /// Finalizer implements dispose pattern.
            /// </summary>
            ~TempFile()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.

                // TODO: Add trace spew for finalize before dispose.
                //       Throwing from a finalizer is a bad idea.

                // Potential performance hit ensues if finalizer called before Dispose(),
                // but we do it anyway.
                Dispose(false);
            }

            /// <summary>
            /// Implements the dispose pattern.
            /// </summary>
            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                // Because we override the finalizer.
                GC.SuppressFinalize(this);
            }
            #endregion

            #region Constructors

            /// <summary>
            /// Default constructor creates new GUID string that is used to create
            /// an extensionless file in the Path.GetTempPath() directory. 
            /// </summary>
            public TempFile()
            {
                CreateFile("", "", null);
            }

            /// <summary>
            /// Constructor uses fqpn (fully qualified path name) to create a file.
            /// </summary>
            /// <param name="fqpn"></param>
            public TempFile(string fqpn)
            {
                FileInfo = new FileInfo(fqpn);
                FileStream = FileInfo.Create();
            }

            /// <summary>
            /// Constructor uses the prefix, new GUID string and extension to create
            /// a file in the Path.GetTempPath() directoryInfo.
            /// </summary>
            /// <remarks>
            /// Verifies that prefix and extension do not contain any double-dots ("..") so that
            /// user data can be used safely with this constructor. Other user and application temp
            /// directories cannot be reached into, indirectly.
            /// </remarks>
            /// <param name="prefix">
            /// The string to prefix to the file.
            /// May be empty or null.
            /// </param>
            /// <param name="extension">
            /// The extension to append to the file name.
            /// If leading dot is missing, one will be applied.
            /// May be empty or null.
            /// </param>
            public TempFile(string prefix, string extension)
            {
                ValidateUserData(prefix, extension);
                CreateFile(prefix ?? "", extension ?? "", null);
            }

            /// <summary>
            /// Constructor, creates new GUID string that is used to create
            /// an extensionless file in the path specified by directoryInfo.
            /// </summary>
            /// <param name="directoryInfo"></param>
            public TempFile(DirectoryInfo directoryInfo)
            {
                Verify.IsNotNull(directoryInfo);

                CreateFile("", "", directoryInfo.FullName);
            }

            /// <summary>
            /// Constructor, uses <paramref name="directoryInfo>"/> to place the temp file,
            /// <paramref name="prefix"/> and <paramref name="extension"/> to name it.
            /// </summary>
            /// <param name="directoryInfo"></param>
            /// <param name="prefix"></param>
            /// <param name="extension"></param>
            public TempFile(DirectoryInfo directoryInfo, string prefix, string extension)
            {
                Verify.IsNotNull(directoryInfo);

                CreateFile(prefix ?? "", extension ?? "", directoryInfo.FullName);
            }
            #endregion Constructors

            #region Private methods

            private void CreateFile(string prefix, string extension, string path)
            {
                string pathFileName;

                if (string.IsNullOrEmpty(path))
                {
                    path = Path.GetTempPath();
                }

                do
                {
                    var fileName = new StringBuilder(prefix);
                    fileName.Append(Guid.NewGuid().ToString());
                    if (extension.Length > 0)
                    {
                        if (extension.First() != '.')
                        {
                            fileName.Append('.');
                        }
                        fileName.Append(extension);
                    }
                    pathFileName = Path.Combine(path, fileName.ToString());
                } while (File.Exists(pathFileName));

                FileInfo = new FileInfo(pathFileName);
                FileStream = FileInfo.Create();
            }

            private void ValidateUserData(string prefix, string extension)
            {
                // Prevent user data from redirecting file to any place outside of Path.GetTempPath().
                if (prefix != null)
                {
                    Verify.IsFalse(prefix.Contains(@".."));
                }

                if (extension != null)
                {
                    Verify.IsFalse(extension.Contains(@".."));
                }
            }

            #endregion Private methods

        }
    }
}