﻿// Copyright (c) Joseph W Donahue dba SharperHacks.com
// http://www.sharperhacks.com
//
// Licensed under the terms of the MIT license (https://opensource.org/licenses/MIT). See LICENSE.TXT.
//
//  Contact: coders@sharperhacks.com
// ---------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

using Timer = System.Timers.Timer;

namespace SharperHacks
{
    namespace IO
    {
        // TODO: This is a weird design.  Needs to be modified to allow nested captures (using Id).

        /// <summary>
        /// Console output redirection wrapper with IDisposable implementation.
        /// </summary>
        public class CaptureConsoleOutput : IDisposable
        {
            #region Public

            // TODO: Add clear captured output. Maybe add a DrainOutput property?

            /// <summary>
            /// The captured output.
            /// </summary>
            public String CapturedOutput
            {
                get { return _stringWriter.ToString(); }
            }

            #endregion

            #region Constructors
            /// <summary>
            /// Default constructor.
            /// </summary>
            /// <remarks>
            /// Thread and nested using() block safe.
            /// </remarks>
            public CaptureConsoleOutput() => Initialize(-1); // Infinite wait.

            /// <summary>
            /// Constructor with timeout on internally shared, mutex synchronized resource wait.
            /// </summary>
            /// <remarks>
            /// Thread and nested using() block safe.
            /// Throws TimeoutException if time to wait expires.
            /// </remarks>
            /// <param name="millisecondsToWait"></param>
            public CaptureConsoleOutput(int millisecondsToWait) => Initialize(millisecondsToWait);

            #endregion Constructors

            #region Private

            private static object _staticLock = new object();
            private bool _initialized = false;

            private static Mutex _mutex = new Mutex();

            // The thread that currently owns console output redirection.
            private static int _currentThreadId;

            private static UInt64 _nestedCounted = 0;

            private StringWriter _stringWriter;
            private TextWriter _previousConsoleOut = null;

            private void Initialize(int millisecondsToWait)
            {
                int myThreadId = Thread.CurrentThread.ManagedThreadId;

                lock (_staticLock)
                {
                    if (!_initialized)
                    {
                        _initialized = true;
                    }

                    if (_currentThreadId == myThreadId)
                    {
                        // We're already holding the mutex.
                        _nestedCounted++;
                        Redirect();
                        return;
                    }
                }

                if (!_mutex.WaitOne(millisecondsToWait))
                {
                    throw new TimeoutException("Timed-out waiting on mutex.");
                }

                lock (_staticLock)
                {
                    _currentThreadId = myThreadId;
                }
                Redirect();
            }

            private void Redirect()
            {
                // Redirect console output so we can capture it.
                _stringWriter = new StringWriter();
                lock (_staticLock)
                {
                    _previousConsoleOut = Console.Out;
                    Console.SetOut(_stringWriter);
                }
            }

#endregion Private

#region IDisposable Support

            private bool _disposedValue = false; // To detect redundant calls

            /// <inheritdoc />
            protected virtual void Dispose(bool disposing)
            {
                if (!_disposedValue)
                {
                    _disposedValue = true;
                    if (disposing)
                    {
                        // Restore console output.
                        lock (_staticLock)
                        {
                            if (null != _previousConsoleOut)
                            {
                                Console.SetOut(_previousConsoleOut);
                            }

                            _currentThreadId = 0;

                            if (_nestedCounted == 0)
                            {
                                _mutex.ReleaseMutex();
                            }
                            else
                            {
                                _nestedCounted--;
                            }
                        }
                    }
                }
            }

            /// <inheritdoc />
            ~CaptureConsoleOutput()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(false);
            }

            // This code added to correctly implement the disposable pattern.
            /// <inheritdoc />
            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                GC.SuppressFinalize(this);
            }

#endregion IDisposable Support
        }
    }
}