﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace SharperHacks.IO.UnitTests
{
    [TestClass]
    public class CaptureConsoleOutputSmokeTest
    {
        private object _lock = new object();

        private TextWriter _previousConsoleOut;

        [TestInitialize]
        public void Init()
        {
            _previousConsoleOut = Console.Out;
        }

        [TestCleanup]
        public void Cleanup()
        {
            Console.SetOut(_previousConsoleOut);
        }

        [TestMethod]
        public void SmokeIt()
        {
            lock (_lock)
            {
                var sw = new StringWriter();
                var previousOut = Console.Out;
                Console.SetOut(sw);

                try
                {
                    string line1 = "Line 1.";
                    string line2 = "Line 2.";

                    using (var captured = new CaptureConsoleOutput())
                    {
                        Console.WriteLine(line1);
                        Console.WriteLine(line2);
                        Assert.IsTrue(captured.CapturedOutput.Contains((line1)));
                        Assert.IsTrue(captured.CapturedOutput.Contains((line2)));
                    }

                    using (var captured = new CaptureConsoleOutput(3000))
                    {
                        Console.WriteLine(line1);
                        Console.WriteLine(line2);
                        Assert.IsTrue(captured.CapturedOutput.Contains((line1)));
                        Assert.IsTrue(captured.CapturedOutput.Contains((line2)));
                    }
                }
                finally
                {
                    string done = "Done?";
                    Console.WriteLine(done);
                    Assert.IsTrue(sw.ToString().Contains(done));
                    Console.SetOut(previousOut);
                }
            }
        }

        private static void ThreadEntry()
        {
            string line1 = "Line 1.";
            string line2 = "Line 2.";
            string line3 = "Line 3.";
            string line4 = "Line 4.";

            string ident = (new Guid()).ToString("N");

            using (var capturedOuter = new CaptureConsoleOutput(100))
            {
                Console.WriteLine(line1);
                Console.WriteLine(line2);
                Console.Out.Flush();

                using (var capturedInner = new CaptureConsoleOutput(100))
                {
                    Console.WriteLine(line3);
                    Console.WriteLine(line4);
                    Console.Out.Flush();

                    Assert.IsTrue(capturedInner.CapturedOutput.Contains(line3));
                    Assert.IsTrue(capturedInner.CapturedOutput.Contains(line4));
                    Assert.IsFalse(capturedInner.CapturedOutput.Contains((line1)));
                    Assert.IsFalse(capturedInner.CapturedOutput.Contains((line2)));
                }

                Assert.IsTrue(capturedOuter.CapturedOutput.Contains((line1)));
                Assert.IsTrue(capturedOuter.CapturedOutput.Contains((line2)));
                Assert.IsFalse(capturedOuter.CapturedOutput.Contains((line3)));
                Assert.IsFalse(capturedOuter.CapturedOutput.Contains((line4)));
            }
        }

        [TestMethod]
        public void SmokeThreaded()
        {
            var threads = new List<Thread>();

            lock (_lock)
            {
                for (int count = 0; count < 20; count++)
                {
                    threads.Add(new Thread(ThreadEntry));
                }

                foreach (var thread in threads)
                {
                    thread.Start();
                    Thread.Sleep(1);
                }
            }
        }

        private static bool _timeOutExceptionCaught = false;

        private static void ThisThreadWillCatchTimeoutException()
        {
            try
            {
                using (var captured = new CaptureConsoleOutput(10))
                {
                    Assert.Fail("We should never get here!");
                }
            }
            catch (TimeoutException)
            {
                _timeOutExceptionCaught = true;
            }
        }

        [TestMethod]
        public void CaptureConsoleOutputStringTimeSpan_ThrowsTimeoutException()
        {
            using (var captured = new CaptureConsoleOutput())
            {
                string line1 = "Line 1.";
                string line2 = "Line 2.";

                Console.WriteLine(line1);
                Console.WriteLine(line2);
                Assert.IsTrue(captured.CapturedOutput.Contains((line1)));
                Assert.IsTrue(captured.CapturedOutput.Contains((line2)));

                var thread = new Thread(ThisThreadWillCatchTimeoutException);
                thread.Start();
                while (thread.ThreadState != ThreadState.Stopped)
                {
                    Thread.Sleep(50);
                }
            }
            Assert.IsTrue(_timeOutExceptionCaught);
        }
    }
}