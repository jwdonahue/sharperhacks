﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharperHacks.Diagnostics.Constraints;
using SharperHacks.IO;

namespace SharperHacks
{
    namespace UnitTests
    {
        [TestClass]
        public class TempFileSmokeTests
        {
            private static string CheckExists(TempFile tempFile)
            {
                Verify.IsNotNull(tempFile);
                var filePathName = tempFile.FileInfo.FullName;
                Assert.IsTrue(File.Exists(filePathName));

                return filePathName;
            }
            private static string CheckExistsAndInTempPath(TempFile tempFile)
            {
                var filePathName = CheckExists(tempFile);
                Assert.IsTrue(filePathName.StartsWith(Path.GetTempPath()));

                return filePathName;
            }

            private static string CheckExistsAndInTempDirectory(TempFile tempFile, TempDirectory tempDirectory)
            {
                Verify.IsNotNull(tempDirectory);
                var filePathName = CheckExists(tempFile);
                Assert.IsTrue(File.Exists(filePathName));
                Assert.IsTrue(filePathName.StartsWith(tempDirectory.DirectoryInfo.FullName));

                return filePathName;
            }

            [TestMethod]
            public void Constructor_Default()
            {
                string filePathName;

                using (var tempFile = new TempFile())
                {
                    filePathName = CheckExistsAndInTempPath(tempFile);
                }
                Assert.IsFalse(File.Exists(filePathName));
            }

            [TestMethod]
            public void Constructor_FQPN()
            {
                string fqpn;
                using (var tempDir = new TempDirectory())
                {
                    using (var tempFile = new TempFile(Path.Combine(tempDir.DirectoryInfo.FullName, new Guid().ToString("N"))))
                    {
                        Assert.IsTrue(File.Exists(tempFile.FileInfo.FullName));
                        fqpn = tempFile.FileInfo.FullName;
                    }
                }
                Assert.IsFalse(File.Exists(fqpn));
            }

            [TestMethod]
            public void Constructor_PrefixExtension()
            {
                string filePathName;

                using (var tempFile = new TempFile(null, null))
                {
                    filePathName = CheckExistsAndInTempPath(tempFile);
                    Assert.IsTrue(string.IsNullOrEmpty(tempFile.FileInfo.Extension));
                }
                Assert.IsFalse(File.Exists(filePathName));

                string prefix = "Prefix1";
                using (var tempFile = new TempFile(prefix, null))
                {
                    filePathName = CheckExistsAndInTempPath(tempFile);
                    Assert.IsTrue(tempFile.FileInfo.Name.StartsWith(prefix));
                    Assert.IsTrue(string.IsNullOrEmpty(tempFile.FileInfo.Extension));
                }
                Assert.IsFalse(File.Exists(filePathName));

                string extension = "ext1";
                using (var tempFile = new TempFile(prefix, extension))
                {
                    filePathName = CheckExistsAndInTempPath(tempFile);
                    Assert.IsTrue(tempFile.FileInfo.Name.StartsWith(prefix));
                    Assert.IsTrue(tempFile.FileInfo.Extension.StartsWith('.'));
                    Assert.IsTrue(tempFile.FileInfo.Extension.EndsWith(extension));
                    string[] tokens = tempFile.FileInfo.FullName.Split('.');
                    Assert.AreEqual(2, tokens.Length);
                }
                Assert.IsFalse(File.Exists(filePathName));

                extension = "ext2";
                using (var tempFile = new TempFile(null, extension))
                {
                    filePathName = CheckExistsAndInTempPath(tempFile);
                    Assert.IsTrue(tempFile.FileInfo.Extension.StartsWith('.'));
                    Assert.IsTrue(tempFile.FileInfo.Extension.EndsWith(extension));
                    string[] tokens = tempFile.FileInfo.FullName.Split('.');
                    Assert.AreEqual(2, tokens.Length);
                }
                Assert.IsFalse(File.Exists(filePathName));
            }
#if false
            [TestMethod]
            public void Constructor_PrefixExtension_HasUserDataChecks()
            {
                bool exceptionCaught = false;
                try
                {
                    using (var tempFile = new TempFile(@"..\invalidPrefix", null))
                    {}
                }
                catch (VerifyException)
                {
                    exceptionCaught = true;
                }
                Verify.IsTrue(exceptionCaught);
                exceptionCaught = false;

                try
                {
                    using (var tempFile = new TempFile(null, @"\..\badExtension"))
                    { }
                }
                catch (VerifyException)
                {
                    exceptionCaught = true;
                }
                Verify.IsTrue(exceptionCaught);
            }
#endif
            [TestMethod]
            public void Constructor_DirectoryInfo()
            {
                string filePathName;

                using (var tempDirectory = new TempDirectory())
                {
                    using (var tempFile = new TempFile(tempDirectory.DirectoryInfo))
                    {
                        filePathName = CheckExistsAndInTempDirectory(tempFile, tempDirectory);
                    }
                }
                Assert.IsFalse(File.Exists(filePathName));
            }

            [TestMethod]
            public void Constructor_DirectoryInfoPrefixExtension()
            {
                string prefix = "prefix";
                string extension = "ext";

                string filePathName;

                using (var tempDirectory = new TempDirectory())
                {
                    using (var tempFile = new TempFile(tempDirectory.DirectoryInfo, prefix, extension))
                    {
                        filePathName = CheckExistsAndInTempDirectory(tempFile, tempDirectory);
                        Assert.IsTrue(tempFile.FileInfo.Name.StartsWith(prefix));
                        Assert.IsTrue(tempFile.FileInfo.Extension.EndsWith(extension));
                    }
                }
                Assert.IsFalse(File.Exists(filePathName));
            }

            [TestMethod]
            public void VerifyTempFileIsUseful()
            {
                using (var tempFile = new TempFile())
                {
                    Assert.IsTrue(tempFile.FileStream.CanWrite);
                    Assert.IsTrue(tempFile.FileStream.CanRead);
                    Assert.IsTrue(tempFile.FileStream.CanSeek);

                    var testString = "This is a test.";

                    using (var sw = new StreamWriter(tempFile.FileStream, Encoding.ASCII))
                    {
                        sw.WriteLine(testString);
                    }

                    using (var sr = new StreamReader(tempFile.FileInfo.FullName, Encoding.ASCII))
                    {
                        string line = sr.ReadLine();
                        Assert.AreEqual(testString, line);
                    }
                }
            }

            private static int FinalizerHelperWithDispose()
            {
                using (var tempFile = new TempFile())
                {
                    return GC.GetGeneration(tempFile);
                }
            }

            private static int FinalizerHelperWithoutDispose()
            {
                var tempFile = new TempFile();
                Assert.IsNotNull(tempFile);
                return GC.GetGeneration(tempFile);
            }

            [TestMethod]
            public void Finalizer()
            {
                int generation = FinalizerHelperWithoutDispose();
                GC.Collect(generation, GCCollectionMode.Forced);
                GC.WaitForPendingFinalizers();

                generation = FinalizerHelperWithDispose();
                GC.Collect(generation, GCCollectionMode.Forced);
                GC.WaitForPendingFinalizers();
            }
        }
    }
}