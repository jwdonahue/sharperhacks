using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using SharperHacks.Diagnostics;

namespace SharperHacks.IO.UnitTests
{
    [TestClass]
    public class TempDirectorySmokeTests
    {
        [TestMethod]
        public void Constructor_Default()
        {
            string directoryPath;

            using (var tmpDir = new TempDirectory())
            {
                directoryPath = tmpDir.DirectoryInfo.FullName;
                Assert.IsTrue(Directory.Exists(directoryPath));
            }

            Assert.IsFalse(Directory.Exists(directoryPath));
        }

        [TestMethod]
        public void Constructor_Prefix()
        {
            string prefix = "SmokeTest-ConstructorPrefix";
            string directoryPath;

            using (var tmpDir = new TempDirectory(prefix))
            {
                directoryPath = tmpDir.DirectoryInfo.FullName;
                Assert.IsTrue(Directory.Exists(directoryPath));
                Assert.IsTrue(directoryPath.Contains(prefix));
            }

            Assert.IsFalse(Directory.Exists(directoryPath));
        }

        [TestMethod]
        [ExpectedException(typeof(VerifyException))]
        public void Constructor_RelativePathPrefixRejected()
        {
            string prefix = @"..\deleteMe";
            using (var tmpDir = new TempDirectory(prefix))
            { } // Should be unreachable.
        }

        [TestMethod]
        public void CreateSubdirectories()
        {
            string prefix = "SmokeTest-CreateSubdirectories";
            string subDirectoryName = "SmokeTest-NamedSubDirectory";
            string directoryPath;
            var subdirectories = new List<string>();

            using (var tmpDir = new TempDirectory(prefix))
            {
                directoryPath = tmpDir.DirectoryInfo.FullName;
                Assert.IsTrue(Directory.Exists(directoryPath));
                Assert.IsTrue(directoryPath.Contains(prefix));

                subdirectories.Add(tmpDir.CreateSubdirectory().FullName);
                subdirectories.Add(tmpDir.CreateSubdirectory(prefix).FullName);
                subdirectories.Add(tmpDir.CreateNamedSubdirectory(subDirectoryName).FullName);

                foreach (var path in subdirectories)
                {
                    Assert.IsTrue(Directory.Exists(path));
                }
            }

            foreach (var path in subdirectories)
            {
                Assert.IsFalse(Directory.Exists(path));
            }

            Assert.IsFalse(Directory.Exists(directoryPath));
        }

        private static int FinalizerHelperWithDispose()
        {
            using (var tempDirectory = new TempDirectory())
            {
                return GC.GetGeneration(tempDirectory);
            }
        }

        private static int FinalizerHelperWithoutDispose()
        {
            var tempDirectory = new TempDirectory();
            Assert.IsNotNull(tempDirectory);
            return GC.GetGeneration(tempDirectory);
        }

        [TestMethod]
        public void Finalizer()
        { 
            int generation = FinalizerHelperWithoutDispose();
            GC.Collect(generation, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();

            generation = FinalizerHelperWithDispose();
            GC.Collect(generation, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();
        }
    }
}
